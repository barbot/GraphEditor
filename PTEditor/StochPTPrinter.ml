open Type
open PetriNet
open StochasticPetriNet
open Format

let string_of_cmpGrML = function
  | EQ -> "equal"
  | NEQ -> "notEqual"
  | LE -> "lessOrEqual"
  | SL -> "stricltyLess"
  | GE -> "greaterOrEqual"
  | SG -> "strictlyGreater"

let print_at f x fy v =
  Format.fprintf f "<attribute name=\"%s\">@[<hov 2>%a@]</attribute>" x fy v

let rec print_expr : type a. Format.formatter -> a expr' -> unit =
 fun f x ->
  let pa x f = print_at f x print_expr in
  let pa2 x f =
    print_at f x (fun f (v1, v2) ->
        Format.fprintf f "\n\t%a\n\t%a\n" print_expr v1 print_expr v2)
  in
  let open Format in
  match x with
  | IntName n -> print_at f "name" pp_print_string n
  | FloatName n -> print_at f "name" pp_print_string n
  | BoolName n -> print_at f "name" pp_print_string n
  | CastBool e -> print_expr f e
  | Int fl -> print_at f "numValue" (fun f -> fprintf f "%i") fl
  | Float fl -> print_at f "numValue" (fun f -> fprintf f "%f") fl
  | Bool fl -> print_at f "boolValue" (fun f -> fprintf f "%B") fl
  | Not fe -> print_at f "function" (pa "not") fe
  | And (fe1, fe2) -> print_at f "function" (pa2 "and") (fe1, fe2)
  | Or (fe1, fe2) -> print_at f "function" (pa2 "or") (fe1, fe2)
  | Mult (fe1, fe2) -> print_at f "function" (pa2 "*") (fe1, fe2)
  | Plus (fe1, fe2) -> print_at f "function" (pa2 "+") (fe1, fe2)
  | Minus (fe1, fe2) -> print_at f "function" (pa2 "minus") (fe1, fe2)
  | Ceil fe -> print_at f "function" (pa "ceil") fe
  | Floor fe -> print_at f "function" (pa "floor") fe
  | CastInt ie -> print_expr f ie
  | Exp fe -> print_at f "function" (pa "exp") fe
  | Div (fe1, fe2) -> print_at f "function" (pa2 "/") (fe1, fe2)
  | FunCall (s, fl) ->
      print_at f "function"
        (fun f ->
          print_at f s (fun f l ->
              List.iter
                (fun x ->
                  print_expr f x;
                  pp_print_string f "\n  ")
                l))
        fl
  | IntAtom (fe1, cmp, fe2) ->
      print_at f "function" (pa2 (string_of_cmpGrML cmp)) (fe1, fe2)
  | FloatAtom (fe1, cmp, fe2) ->
      print_at f "function" (pa2 (string_of_cmpGrML cmp)) (fe1, fe2)
  | BoolAtom (fe1, cmp, fe2) ->
      print_at f "function" (pa2 (string_of_cmpGrML cmp)) (fe1, fe2)
  | If (c, e1, e2) ->
      print_at f "function"
        (fun f ->
          print_at f "If" (fun f () ->
              print_expr f c;
              pp_print_string f "\n  ";
              print_expr f e1;
              pp_print_string f "\n  ";
              print_expr f e2;
              pp_print_string f "\n  "))
        ()

let print_int_expr f (x : int expr') = print_expr f x

let print_float_expr f (x : float expr') = print_expr f x

let print_distr f d =
  let open Format in
  pp_print_string f "      <attribute name=\"type\">\n";
  match d with
  | Exp r ->
      fprintf f
        "        EXPONENTIAL\n\
        \      </attribute>\n\
        \      <attribute name=\"param\">\n\
        \        <attribute name=\"number\">0</attribute>\n\
        \        <attribute name=\"expr\">%a</attribute>\n\
        \      </attribute>" print_expr r
  | Synchronisation s ->
      fprintf f
        "        SYNC\n\
        \      </attribute>\n\
        \      <attribute name=\"param\">\n\
        \        <attribute name=\"number\">0</attribute>\n\
        \        <attribute name=\"expr\"><attribute name=\"unParsed\">\n\
        \          %s\n\
        \        </attribute></attribute>\n\
        \      </attribute>" s
  | Player1 ->
      fprintf f
        "        PLAYER1\n\
        \      </attribute>\n\
        \      <attribute name=\"param\">\n\
        \        <attribute name=\"number\">0</attribute>\n\
        \        <attribute name=\"expr\"><attribute name=\"numValue\">\n\
        \          0\n\
        \        </attribute></attribute>\n\
        \      </attribute>"
  | Erl (i, r) ->
      fprintf f
        "        ERLANG\n\
        \      </attribute>\n\
        \      <attribute name=\"param\">\n\
        \        <attribute name=\"number\">0</attribute>\n\
        \        <attribute name=\"expr\">%a</attribute>\n\
        \      </attribute>\n\
        \      <attribute name=\"param\">\n\
        \        <attribute name=\"number\">1</attribute>\n\
        \        <attribute name=\"expr\">%a</attribute>\n\
        \      </attribute>" print_expr i print_expr r
  | DiscUnif i ->
      fprintf f
        "        DISCRETEUNIF\n\
        \      </attribute>\n\
        \      <attribute name=\"param\">\n\
        \        <attribute name=\"number\">0</attribute>\n\
        \        <attribute name=\"expr\">%a</attribute>\n\
        \      </attribute>" print_expr i
  | Unif (l, h) ->
      fprintf f
        "        UNIFORM\n\
        \      </attribute>\n\
        \      <attribute name=\"param\">\n\
        \        <attribute name=\"number\">0</attribute>\n\
        \        <attribute name=\"expr\">%a</attribute>\n\
        \      </attribute>\n\
        \      <attribute name=\"param\">\n\
        \        <attribute name=\"number\">1</attribute>\n\
        \        <attribute name=\"expr\">%a</attribute>\n\
        \      </attribute>" print_expr l print_expr h
  | LogNormal (l, h) ->
      fprintf f
        "        LOGNORMAL\n\
        \      </attribute>\n\
        \      <attribute name=\"param\">\n\
        \        <attribute name=\"number\">0</attribute>\n\
        \        <attribute name=\"expr\">%a</attribute>\n\
        \      </attribute>\n\
        \      <attribute name=\"param\">\n\
        \        <attribute name=\"number\">1</attribute>\n\
        \        <attribute name=\"expr\">%a</attribute>\n\
        \      </attribute>" print_expr l print_expr h
  | Geometric (l, h) ->
      fprintf f
        "        GEOMETRIC\n\
        \      </attribute>\n\
        \      <attribute name=\"param\">\n\
        \        <attribute name=\"number\">0</attribute>\n\
        \        <attribute name=\"expr\">%a</attribute>\n\
        \      </attribute>\n\
        \      <attribute name=\"param\">\n\
        \        <attribute name=\"number\">1</attribute>\n\
        \        <attribute name=\"expr\">%a</attribute>\n\
        \      </attribute>" print_expr l print_expr h
  | Imm ->
      fprintf f
        "        DETERMINISTIC\n\
        \      </attribute>\n\
        \      <attribute name=\"param\">\n\
        \        <attribute name=\"number\">0</attribute>\n\
        \        <attribute name=\"expr\"><attribute name=\"numValue\">\n\
        \          0\n\
        \        </attribute></attribute>\n\
        \      </attribute>"
  | Det p ->
      fprintf f
        "        DETERMINISTIC\n\
        \      </attribute>\n\
        \      <attribute name=\"param\">\n\
        \        <attribute name=\"number\">0</attribute>\n\
        \        <attribute name=\"expr\">\n\
        \          %a\n\
        \        </attribute>\n\
        \      </attribute>" print_expr p
  | Norm (m, v) ->
      fprintf f
        "        NORMAL\n\
        \      </attribute>\n\
        \      <attribute name=\"param\">\n\
        \        <attribute name=\"number\">0</attribute>\n\
        \        <attribute name=\"expr\">%a</attribute>\n\
        \      </attribute>\n\
        \      <attribute name=\"param\">\n\
        \        <attribute name=\"number\">1</attribute>\n\
        \        <attribute name=\"expr\">%a</attribute>\n\
        \      </attribute>" print_expr m print_expr v
  | DiscUserDef i ->
      fprintf f
        "        DISCRETEUSERDEFINE\n\
        \      </attribute>\n\
        \      <attribute name=\"param\">\n\
        \        <attribute name=\"number\">0</attribute>\n\
        \        <attribute name=\"expr\"><attribute \
         name=\"numValue\">%i</attribute></attribute>\n\
        \      </attribute>" i

let printH_distr f = function
  | Imm -> Format.fprintf f "DETERMINISTIC(0.0)"
  | Det r -> Format.fprintf f "DETERMINISTIC(%a)" printH_expr r
  | Exp r -> Format.fprintf f "EXPONENTIAL(%a)" printH_expr r
  | Erl (i, r) -> Format.fprintf f "ERLANG(%a,%a)" printH_expr i printH_expr r
  | Unif (i, r) -> Format.fprintf f "UNIFORM(%a,%a)" printH_expr i printH_expr r
  | LogNormal (i, r) ->
      Format.fprintf f "LOGNORMAL(%a,%a)" printH_expr i printH_expr r
  | DiscUnif i -> Format.fprintf f "DISCRETEUNI(%a)" printH_expr i
  | Geometric (i, r) ->
      Format.fprintf f "GEOMETRIC(%a,%a)" printH_expr i printH_expr r
  | Norm (m, v) -> Format.fprintf f "NORMAL(%a,%a)" printH_expr m printH_expr v
  | DiscUserDef i -> Format.fprintf f "EXPONENTIAL(%i)" i
  | Player1 -> Format.fprintf f "PLAYER1()"
  | Synchronisation s -> Format.fprintf f "SYNC(%s)" s

let printH_distr_CTMC f = function
  | Imm -> Format.fprintf f "I"
  | Exp r -> Format.fprintf f "%a" printH_expr r
  | d -> printH_distr f d

let print_tr f name id tr =
  Format.fprintf f
    "  <node id=\"%i\" nodeType=\"transition\">\n\
    \    <attribute name=\"name\">%s</attribute>\n\
    \      <attribute name=\"distribution\">\n\
    \        %a\n\
    \      </attribute>\n\
    \      <attribute name=\"priority\"><attribute name=\"expr\">\n\
    \        %a\n\
    \      </attribute></attribute>\n\
    \      <attribute name=\"weight\"><attribute name=\"expr\">\n\
    \        %a\n\
    \      </attribute></attribute>\n\
    \  </node>\n"
    id name print_distr tr.distribution print_float_expr tr.prio
    print_float_expr tr.weight

let print_pl f name id pl =
  Format.fprintf f
    "  <node id=\"%i\" nodeType=\"place\">\n\
    \    <attribute name=\"name\">%s</attribute>\n\
    \    <attribute name=\"marking\"><attribute name=\"expr\">\n\
    \      %a\n\
    \    </attribute></attribute>\n\
    \  </node>\n"
    id name print_int_expr pl.marking

let print_arc f id source target valuation inhib =
  let arctype = if inhib then "inhibitorarc" else "arc" in
  Format.fprintf f
    "  <arc id=\"%i\" arcType=\"%s\" source=\"%i\" target=\"%i\">\n\
    \    <attribute name=\"valuation\"><attribute name=\"expr\">\n\
    \        %a\n\
    \    </attribute></attribute>\n\
    \  </arc>\n"
    id arctype source target print_int_expr valuation

let gen_const f li lr le fund classdef =
  let open Format in
  fprintf f
    "%a\n\
    \  <attribute name=\"declaration\"><attribute name=\"constants\">\n\
    \    <attribute name=\"intConsts\">\n"
    fund ();
  List.iter
    (fun (n, v) ->
      fprintf f
        "      <attribute name=\"intConst\">\n\
        \        <attribute name=\"name\">%s</attribute>\n\
        \        <attribute name=\"expr\"><attribute name=\"numValue\">\n\
        \            %a\n\
        \        </attribute></attribute>\n\
        \      </attribute>\n"
        n printH_expr v)
    li;

  fprintf f "    </attribute>\n    <attribute name=\"realConsts\">\n";
  List.iter
    (fun (n, v) ->
      fprintf f
        "      <attribute name=\"realConst\">\n\
        \        <attribute name=\"name\">%s</attribute>\n\
        \        <attribute name=\"expr\"><attribute name=\"numValue\">\n\
        \            %a\n\
        \        </attribute></attribute>\n\
        \      </attribute>\n"
        n printH_expr v)
    lr;
  fprintf f "     </attribute>\n    <attribute name=\"extConsts\">\n";
  List.iter
    (fun n ->
      fprintf f
        "      <attribute name=\"extConst\">\n\
        \        <attribute name=\"name\">%s</attribute>\n\
        \      </attribute>\n"
        n)
    le;

  fprintf f "    </attribute>\n  </attribute>\n";
  List.iter (fun s -> fprintf f "%s\n" s) classdef;
  fprintf f "</attribute>\n"

let print_declaration f def =
  let lci, lcd, lce, fund, classDef =
    match def with
    | None -> ([], [], [], (fun _ () -> ()), [])
    | Some a -> (a.intconst, a.floatconst, a.clock, a.printer, a.classDef)
  in
  gen_const f
    (List.map
       (fun (s, ao) -> match ao with None -> (s, Int 1) | Some fl -> (s, fl))
       lci)
    (List.map
       (fun (s, ao) ->
         match ao with None -> (s, Float 1.0) | Some fl -> (s, fl))
       lcd)
    lce fund classDef

let print_general_pt ?(initid = 0) f placePrinter transitionPrinter
    valuationPrinter declarationPrinter net =
  Format.fprintf f "<?xml version=\"1.0\" encoding=\"UTF-8\"?>@;";
  Format.fprintf f
    "<model formalismUrl=\"http://formalisms.cosyverif.org/sptgd-net.fml\" \
     xmlns=\"http://cosyverif.org/ns/model\">@[<hov 2>@;";
  declarationPrinter f net.Net.def;
  let np =
    Data.fold
      (fun i (s, m) ->
        placePrinter f s i m;
        i + 1)
      initid net.Net.place
  in
  let nt =
    Data.fold
      (fun i (s, r) ->
        transitionPrinter f s i r;
        i + 1)
      np net.Net.transition
  in
  let nia =
    Data.fold
      (fun i (_, (v, p, t)) ->
        valuationPrinter f i (Obj.magic p) (Obj.magic t + np) v false;
        i + 1)
      nt net.Net.inArc
  in
  let nio =
    Data.fold
      (fun i (_, (v, t, p)) ->
        valuationPrinter f i (Obj.magic t + np) (Obj.magic p) v false;
        i + 1)
      nia net.Net.outArc
  in
  let _ =
    Data.fold
      (fun i (_, (v, p, t)) ->
        valuationPrinter f i (Obj.magic p) (Obj.magic t + np) v true;
        i + 1)
      nio net.Net.inhibArc
  in
  ();
  Format.fprintf f "@]</model>"

let print_spt fpath (net : spt) =
  let fstd = open_out fpath in
  let f = formatter_of_out_channel fstd in
  print_general_pt f print_pl print_tr print_arc print_declaration net;
  close_out fstd

let print_pnml_arc f id source target valuation =
  fprintf f
    "      <arc id=\"A_%i\" source=\"PT_%i\" target=\"PT_%i\">\n\
    \        <inscription><text>%a</text></inscription>\n\
    \      </arc>\n"
    id source target printH_expr valuation

let print_pnml fpath (net : spt) =
  let fstd = open_out fpath in
  let f = formatter_of_out_channel fstd in
  fprintf f
    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\
     <pnml xmlns=\"http://www.pnml.org/version-2009/grammar/pnml\">\n\
    \  <net id=\"Generated-By-Cosmos-0001\" \
     type=\"http://www.pnml.org/version-2009/grammar/ptnet\">\n\
    \    <page id=\"page0\">\n\
    \      <name><text>DefaultPage</text></name>\n";
  let np =
    Data.fold
      (fun i (s, pl) ->
        fprintf f
          "      <place id=\"PT_%i\">\n\
          \        <name><text>%s</text></name>\n\
          \        <initialMarking><text>%a</text></initialMarking>\n\
          \      </place>\n"
          i s printH_expr pl.marking;
        i + 1)
      0 net.Net.place
  in
  let nt =
    Data.fold
      (fun i (s, _) ->
        fprintf f
          "      <transition id=\"PT_%i\">\n\
          \        <name><text>%s</text></name>\n\
          \      </transition>\n"
          i s;
        i + 1)
      np net.Net.transition
  in
  let nia =
    Data.fold
      (fun i (_, (v, p, t)) ->
        print_pnml_arc f i (Obj.magic p) (Obj.magic t + np) v;
        i + 1)
      nt net.Net.inArc
  in
  let nio =
    Data.fold
      (fun i (_, (v, t, p)) ->
        print_pnml_arc f i (Obj.magic t + np) (Obj.magic p) v;
        i + 1)
      nia net.Net.outArc
  in
  let _ =
    Data.fold
      (fun _ _ -> failwith "inhibitor Arc not supported in PNML")
      nio net.Net.inhibArc
  in
  ();
  fprintf f "    </page>\n  </net>\n</pnml>";
  close_out fstd

let rec inline_ls d = function
  | [] -> ""
  | [ s ] -> s
  | t :: q -> t ^ d ^ inline_ls d q

let print_arc_marcie net f t =
  let fq =
    Data.fold
      (fun first (_, (v, t2, p)) ->
        if t <> t2 then first
        else (
          if not first then fprintf f " & ";
          fprintf f "[%s + %a]" (fst (Data.acca net.Net.place p)) printH_expr v;
          false ))
      true net.Net.outArc
  in
  let fq2 =
    Data.fold
      (fun first (_, (v, p, t2)) ->
        if t <> t2 then first
        else (
          if not first then fprintf f " & ";
          fprintf f "[%s - %a]" (fst (Data.acca net.Net.place p)) printH_expr v;
          false ))
      fq net.Net.inArc
  in
  ignore fq2

let print_condition_arc_marcie net f t =
  let fq3 =
    Data.fold
      (fun first (_, (v, p, t2)) ->
        if t <> t2 then first
        else (
          if not first then fprintf f " & ";
          fprintf f "[%s < %a]" (fst (Data.acca net.Net.place p)) printH_expr v;
          false ))
      true net.Net.inhibArc
  in
  ignore fq3

let print_spt_marcie fpath net =
  let fstd = open_out fpath in
  let f = Format.formatter_of_out_channel fstd in
  pp_print_string f "gspn [generated_cosmos] {\n";

  fprintf f "constants:\n";
  let lci, lcd, _, _ =
    match net.Net.def with
    | None -> ([], [], [], fun _ () -> ())
    | Some a -> (a.intconst, a.floatconst, a.clock, a.printer)
  in
  List.iter
    (fun (s, ao) ->
      match ao with
      | None -> fprintf f "\tint %s;\n" s
      | Some iv -> fprintf f "\tint %s=%a;\n" s printH_expr iv)
    lci;
  List.iter
    (fun (s, ao) ->
      match ao with
      | None -> fprintf f "\tint %s;\n" s
      | Some fv -> fprintf f "\tdouble %s=%a;\n" s printH_expr fv)
    lcd;

  pp_print_string f "places:\n";
  Data.iter
    (fun (s, pl) -> fprintf f "\t%s = %a;\n" s printH_expr pl.marking)
    net.Net.place;

  pp_print_string f "\ntransitions:\n";
  pp_print_string f "\tstochastic:\n";
  Data.iter
    (fun (s, tr) ->
      match tr.distribution with
      | Exp r ->
          fprintf f "\t%s : %a : %a : %a ;\n" s
            (print_condition_arc_marcie net)
            (Data.index net.Net.transition s)
            (print_arc_marcie net)
            (Data.index net.Net.transition s)
            printH_expr r
      | Det r ->
          fprintf f "\t%s : %a : %a : %a ;\n" s
            (print_condition_arc_marcie net)
            (Data.index net.Net.transition s)
            (print_arc_marcie net)
            (Data.index net.Net.transition s)
            printH_expr r
      | Erl (n, r) ->
          fprintf f "\t%s : %a : %a : %a ;\n" s
            (print_condition_arc_marcie net)
            (Data.index net.Net.transition s)
            (print_arc_marcie net)
            (Data.index net.Net.transition s)
            printH_expr
            (Div (r, CastInt n))
      | _ -> ())
    net.Net.transition;
  pp_print_string f "\timmediate:\n";
  Data.iter
    (fun (s, tr) ->
      match tr.distribution with
      | Imm ->
          fprintf f "\t%s : %a : %a : %a ;\n" s
            (print_condition_arc_marcie net)
            (Data.index net.Net.transition s)
            (print_arc_marcie net)
            (Data.index net.Net.transition s)
            printH_expr tr.weight
      | Player1 ->
          fprintf f "\t%s : %a : %a : %a ;\n" s
            (print_condition_arc_marcie net)
            (Data.index net.Net.transition s)
            (print_arc_marcie net)
            (Data.index net.Net.transition s)
            printH_expr tr.weight
      | _ -> ())
    net.Net.transition;

  pp_print_string f "}\n";
  close_out fstd

let print_arc_dot f s1 s2 v =
  if v <> Int 1 then
    fprintf f "\t%s -> %s [xlabel=\"%a\"];\n" s1 s2 printH_expr v
  else fprintf f "\t%s -> %s;\n" s1 s2

let print_inhib_arc_dot f s1 s2 v =
  if v <> Int 1 then
    fprintf f "\t%s -> %s [arrowhead=odot,xlabel=\"  %a  \"];\n" s1 s2
      printH_expr v
  else fprintf f "\t%s -> %s [arrowhead=odot];\n" s1 s2

let colorOfTrans = function
  | Exp (Float r) -> (
      match r /. 0.009 with
      | x when x >= 1. -> "\"#505050\""
      | x when x >= 1. /. 50. -> "\"#919191\""
      | _ -> "\"#D0D0D0\"" )
  | _ -> "black"

let print_spt_dot ?(showlabel = true) fpath net cl p =
  let fstd = open_out fpath in
  let f = Format.formatter_of_out_channel fstd in

  pp_print_string f
    "digraph G {\n\n\tnode[fontsize=18];\n\tedge[fontsize=18];\n";

  (* pp_print_string f "\tsubgraph place {\n";
     pp_print_string f "\t\tgraph [shape=circle];\n";
     pp_print_string f "\t\tnode [shape=circle,fixedsize=true];\n";*)
  Data.iter
    (fun (s, pl) ->
      let pos =
        try
          let x, y = List.assoc s p in
          sprintf ",pos=\"%f,%f!\"" (0.75 *. x) (0.75 *. y)
        with Not_found -> ""
      in

      fprintf f "\t%s [shape=circle,xlabel=\"%s\",label=\"%a\"%s];\n" s
        (if showlabel then s else "")
        print_token pl.marking pos)
    net.Net.place;
  (*pp_print_string f "\t}\n\tsubgraph transition {\n";
    pp_print_string f "\t\tnode [shape=rect,fixedsize=true,height=0.2,style=filled,fillcolor=black];\n";*)
  Data.iter
    (fun (s, tr) ->
      let pos =
        try
          let x, y = List.assoc s p in
          sprintf ",pos=\"%f,%f!\"" (0.75 *. x) (0.75 *. y)
        with Not_found -> ""
      in
      fprintf f
        "\t%s \
         [shape=rect,fixedsize=true,height=0.12,width=0.5,style=filled,fillcolor=%s,xlabel=\"%s%a\",label=\"\"%s];\n"
        s
        (colorOfTrans tr.distribution)
        (if showlabel then s ^ ":" else "")
        (fun o v -> if showlabel then printH_distr_CTMC o v else ())
        tr.distribution pos)
    net.Net.transition;
  (*pp_print_string f "\t}\n";*)
  Data.iter
    (fun (_, (v, p, t)) ->
      print_arc_dot f
        (fst (Data.acca net.Net.place p))
        (fst (Data.acca net.Net.transition t))
        v)
    net.Net.inArc;
  Data.iter
    (fun (_, (v, p, t)) ->
      print_inhib_arc_dot f
        (fst (Data.acca net.Net.place p))
        (fst (Data.acca net.Net.transition t))
        v)
    net.Net.inhibArc;
  Data.iter
    (fun (_, (v, t, p)) ->
      print_arc_dot f
        (fst (Data.acca net.Net.transition t))
        (fst (Data.acca net.Net.place p))
        v)
    net.Net.outArc;

  List.iter
    (fun (s, l) ->
      fprintf f "\tsubgraph \"test%i\" {\n\trank=%i; " s s;
      List.iter (fun s2 -> fprintf f " %s;" s2) l;
      pp_print_string f "}\n")
    cl;
  pp_print_string f "}\n";
  close_out fstd

let display_dot net =
  let file = "tmpgraph" in
  print_spt_dot (file ^ ".dot") net [] [];
  ignore @@ Sys.command (sprintf "dot -Tpdf %s.dot -o %s.pdf" file file);
  ignore @@ Sys.command (sprintf "open  %s.pdf" file)
