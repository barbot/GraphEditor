(*
#mod_use "type.ml"
#mod_use "PetriNet.ml"
*)

open Type
open PetriNet

type distr =
  | Exp of float expr'
  | Imm
  | Unif of (float expr' * float expr')
  | DiscUnif of int expr'
  | Norm of (float expr' * float expr')
  | Det of float expr'
  | Erl of (int expr' * float expr')
  | Geometric of (float expr' * float expr')
  | LogNormal of (float expr' * float expr')
  | DiscUserDef of int
  | Player1
  | Synchronisation of string

type position = (float * float) option

type placetypeSPT = {
  marking : int Type.expr';
  bound : int Type.expr' option;
  positionP : position;
}

let bp (m, b) = { marking = m; bound = b; positionP = None }

type valuationtypeSPT = int Type.expr'

type transitiontypeSPT = {
  distribution : distr;
  weight : float Type.expr';
  prio : float Type.expr';
  positionT : position;
}

let bt (d, w, p) = { distribution = d; weight = w; prio = p; positionT = None }

type declarationSPT = {
  mutable intconst : (string * int Type.expr' option) list;
  mutable floatconst : (string * float Type.expr' option) list;
  mutable clock : string list;
  mutable printer : Format.formatter -> unit -> unit;
  mutable labels : (string * bool Type.expr') list;
  mutable formulas : (string * Type.full_expr) list;
  mutable classDef : string list;
}

type spt =
  ( placetypeSPT,
    (* Initial Marking type and bound on place*)
    transitiontypeSPT,
    (* Label of transitions type *)
  int Type.expr',
    (* valuation of arcs *)
    declarationSPT (* Initialization *) )
  Net.t

open Net

let eval_spt spt e =
  eval
    ~iname:(fun x ->
      (spt.def |>> fun s -> Some s.intconst) |>> fun s ->
      try List.assoc x s with _ -> None)
    ~fname:(fun x ->
      (spt.def |>> fun s -> Some s.floatconst) |>> fun s ->
      try List.assoc x s with _ -> None)
    e

let eval_or_die_spt spt e =
  eval_or_die
    ~iname:(fun x ->
      (spt.def |>> fun s -> Some s.intconst) |>> fun s ->
      try List.assoc x s with _ -> None)
    ~fname:(fun x ->
      (spt.def |>> fun s -> Some s.floatconst) |>> fun s ->
      try List.assoc x s with _ -> None)
    e

(* Print as a prism CTMC model*)
let print_prism_module fpath net =
  let fstd = open_out fpath in
  let f = Format.formatter_of_out_channel fstd in
  Format.fprintf f "ctmc\nconst double imm=100000;\n";
  Format.fprintf f "module m1\n";
  Data.iter
    (fun (n, x) ->
      Format.fprintf f "\t%s: [%i..%i] init %a;\n" n 0 2 printH_expr x)
    net.place;

  Data.iteri
    (fun i (_, x) ->
      ( match x with
      | Imm, _, _ -> Format.fprintf f "\t[] "
      | _ -> Format.fprintf f "\t[step] " );
      let prims =
        Data.fold
          (fun b (_, (s, p, t)) ->
            if t = i then (
              if b then Format.fprintf f " & ";
              Format.fprintf f "(%s>=%a)"
                (Data.acca net.place p |> fst)
                printH_expr s;
              true )
            else b)
          false net.inArc
      in
      ignore
      @@ Data.fold
           (fun b (_, (s, p, t)) ->
             if t = i then (
               if b then Format.fprintf f " & ";
               Format.fprintf f "(%s<%a)"
                 (Data.acca net.place p |> fst)
                 printH_expr s;
               true )
             else b)
           prims net.inhibArc;

      ( match x with
      | Imm, w, _ -> Format.fprintf f " -> imm*%a : " printH_expr w
      | Exp s, _, _ -> Format.fprintf f " -> %a : " printH_expr s
      | _ -> failwith "Prism does not support distribution shape" );

      let update = Data.create () in

      Data.iteri
        (fun ip _ ->
          Data.iter
            (fun (_, (s, t, p)) ->
              if t = i && p = ip then Data.add (p, s) update)
            net.outArc;
          Data.iter
            (fun (_, (s, p, t)) ->
              if t = i && p = ip then
                try
                  let iup = Data.index update p in
                  let v2 = eval (Minus (Data.acca update iup |> snd, s)) in
                  Data.updatea iup v2 update
                with Data.Empty | Not_found ->
                  Data.add (p, eval (Minus (Int 0, s))) update)
            net.inArc)
        net.place;

      ignore
      @@ Data.fold
           (fun b (p, v) ->
             if b then Format.fprintf f " & ";
             let pname = Data.acca net.place p |> fst in
             Format.fprintf f "(%s'=%a)" pname printH_expr
               (Plus (IntName pname, v));
             true)
           false update;
      Format.fprintf f ";\n")
    net.transition;
  Format.fprintf f "endmodule\n";
  Format.fprintf f "rewards \"steps\"\n\t[step] true : 1;\nendrewards";
  close_out fstd

let remove_erlang (net : spt) =
  let net2 =
    {
      net with
      Net.def = net.Net.def;
      transition = Data.copy net.transition;
      inArc = Data.copy net.inArc;
      outArc = Data.copy net.outArc;
      inhibArc = Data.copy net.inhibArc;
    }
  in
  Data.iter
    (fun (y, tr) ->
      match tr.distribution with
      | Erl (n, lamb) ->
          ignore
          @@ Data.add ("P" ^ y ^ "ErCont", bp (Int 0, Some n)) net2.Net.place;
          ignore
          @@ Data.add
               (y ^ "ErStep", { tr with distribution = Exp lamb })
               net2.Net.transition;
          Net.add_inArcS net2 ("P" ^ y ^ "ErCont") y n;
          Net.add_inhibArcS net2 ("P" ^ y ^ "ErCont") (y ^ "ErStep") n;
          Net.add_outArcS net2 (y ^ "ErStep") ("P" ^ y ^ "ErCont") (Int 1);
          let it = Data.index net.Net.transition y in
          Data.iter
            (fun ((), (v, p, t)) ->
              if t = it then (
                let pn, _ = Data.acca net.Net.place p in
                let emptyn = y ^ "ErEmptyIn" ^ pn in
                Data.add
                  ( emptyn,
                    {
                      tr with
                      distribution = Imm;
                      prio = Plus (tr.prio, Float 4.0);
                    } )
                  net2.Net.transition;
                Net.add_inArcS net2 ("P" ^ y ^ "ErCont") emptyn (Int 1);
                Net.add_outArcS net2 (y ^ "ErStep") pn v;
                Net.add_inArcS net2 pn (y ^ "ErStep") v;
                Net.add_inhibArcS net2 pn emptyn v ))
            net.Net.inArc;
          Data.iter
            (fun ((), (v, p, t)) ->
              if t = it then (
                let pn, _ = Data.acca net.Net.place p in
                let emptyn = y ^ "ErEmptyInhib" ^ pn in
                Data.add
                  ( emptyn,
                    {
                      tr with
                      distribution = Imm;
                      prio = Plus (tr.prio, Float 4.0);
                    } )
                  net2.Net.transition;
                Net.add_inArcS net2 ("P" ^ y ^ "ErCont") emptyn (Int 1);
                Net.add_inhibArcS net2 pn (y ^ "ErStep") v;
                Net.add_inArcS net2 pn emptyn v;
                Net.add_outArcS net2 emptyn pn v ))
            net.Net.inhibArc
      | _ -> ())
    net.Net.transition;

  let transn2 =
    net2.Net.transition
    |> Data.map (fun name tr ->
           match tr.distribution with
           | Erl (_, _) -> (name, tr)
           | _ -> (name, tr))
  in
  { net2 with Net.transition = transn2 }

let add_reward_struct (net : spt) =
  Data.iter
    (fun (y, tr) ->
      if tr.distribution <> Imm then (
        Data.add
          ("P" ^ y ^ "_RewardStr", bp (Int 0, Some (Int 1)))
          net.Net.place;
        Data.add
          ( "Tr_" ^ y ^ "RewardStr",
            {
              tr with
              distribution = Unif (FloatName (y ^ "min"), FloatName (y ^ "max"));
            } )
          net.Net.transition;
        Net.add_outArcS net y ("P" ^ y ^ "_RewardStr") (Int 1);
        Net.add_inArcS net
          ("P" ^ y ^ "_RewardStr")
          ("Tr_" ^ y ^ "RewardStr")
          (Int 1) ))
    net.Net.transition

let remove_inhibitor (net : spt) =
  let place2 = Data.copy net.place in
  net.place
  |> Data.filter (fun _ pl -> pl.bound <> None)
  |> Data.map (fun name pl ->
         match pl.bound with
         | None -> failwith "Not Possible"
         | Some b ->
             ("A_" ^ name, { pl with marking = eval (Minus (b, pl.marking)) }))
  |> Data.iter (fun pl -> Data.add pl place2);

  let inArc2 = Data.copy net.inArc in
  let outArc2 = Data.copy net.outArc in
  net.place
  |> Data.iteri (fun p (namep, pl) ->
         match pl.bound with
         | Some bm ->
             let p2 = Data.index place2 ("A_" ^ namep) in
             net.transition
             |> Data.iteri (fun t (_, _) ->
                    let inval =
                      net.inArc
                      |> Data.filter (fun _ (_, p3, t3) -> t3 = t && p3 = p)
                      |> Data.fold
                           (fun v1 (_, (v2, _, _)) -> Plus (v1, v2))
                           (Int 0)
                      |> eval
                    and outval =
                      net.outArc
                      |> Data.filter (fun _ (_, t3, p3) -> t3 = t && p3 = p)
                      |> Data.fold
                           (fun v1 (_, (v2, _, _)) -> Plus (v1, v2))
                           (Int 0)
                      |> eval
                    and inhibval =
                      net.inhibArc
                      |> Data.filter (fun _ (_, p3, t3) -> t3 = t && p3 = p)
                      |> Data.fold
                           (fun v1 (_, (v2, _, _)) -> Plus (v1, v2))
                           (Int 0)
                      |> eval
                    in
                    let v2 =
                      if inhibval <> Int 0 then
                        Plus (Int 1, Minus (bm, inhibval))
                      else Int 0
                    in
                    let v3 = Plus (Minus (inval, outval), v2) in
                    let v4 = eval v3 in
                    if v2 <> Int 0 then Data.add ((), (v2, p2, t)) inArc2;
                    if v4 <> Int 0 then Data.add ((), (v4, t, p2)) outArc2)
         | None -> ());

  {
    def = net.def;
    place = place2;
    transition = Data.copy net.transition;
    inArc = inArc2;
    outArc = outArc2;
    inhibArc = Data.create ();
  }
