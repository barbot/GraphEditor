function upload(cb,inputId) {
    var files = document.getElementById(inputId).files;
    
    if (!files.length) {
      alert('Please select a file!');
      return;
    }

    var file = files[0];
   
    var reader = new FileReader();

    // If we use onloadend, we need to check the readyState.
    reader.onloadend = function(evt) {
	if (evt.target.readyState == FileReader.DONE) { // DONE == 2
	    cb(evt.target.result);
      }
    };

    reader.readAsText(file);
  }
  
