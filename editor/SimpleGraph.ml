module type PREGRAPH = sig
  type def
  type state
  type arc
  type attribute_id

  type attribute =
    [ `String of string
    | `Choice of string list
    | `Check of bool
    | `ControlPoint of DrawingGeom.point
    | `Color of string ]

  val init_def : unit -> def
  val init_arc : state -> state -> arc option
  val init_state : unit -> state
  val draw_state : state -> DrawingGeom.point -> DrawingGeom.shape list

  val draw_arc :
    arc -> DrawingGeom.shape * DrawingGeom.shape -> DrawingGeom.shape list

  val get_state_attr :
    state -> string * (attribute_id * string * string option * attribute) list

  val get_new_state_attr :
    state ->
    DrawingGeom.point ->
    (string * (state -> state * attribute_id)) list

  val update_state_attr :
    state -> attribute_id -> attribute option -> state option

  (*    val update_state_attr: state -> string -> string -> bool*)
  val get_new_arc_attr :
    arc ->
    DrawingGeom.point * DrawingGeom.point ->
    (string * (arc -> arc * attribute_id)) list

  val get_arc_attr :
    arc -> string * (attribute_id * string * string option * attribute) list

  val update_arc_attr : arc -> attribute_id -> attribute option -> arc option

  val get_def_attr :
    def -> string * (attribute_id * string * string option * attribute) list

  val update_def_attr : def -> attribute_id -> attribute option -> def option
  val get_new_def_attr : def -> (string * (def -> def * attribute_id)) list

  val parse_file :
    string ->
    (state -> DrawingGeom.point -> 'nodeid) ->
    (arc -> 'nodeid -> 'nodeid -> unit) ->
    unit

  val print :
    (string
    * (Format.formatter ->
      def ->
      ((int -> state -> DrawingGeom.point -> unit) -> unit) ->
      ((int ->
       arc ->
       (int * state * DrawingGeom.point) * (int * state * DrawingGeom.point) ->
       unit) ->
      unit) ->
      unit)
    * string)
    list
end

module S (P : PREGRAPH) = struct
  type attribute_id = P.attribute_id
  type stateKey = unit
  type arcKey = unit

  type graph = {
    mutable def : P.def;
    state : (unit, P.state ref * (float * float) ref, stateKey) Data.t;
    arc :
      (unit, P.arc ref * stateKey Data.key * stateKey Data.key, arcKey) Data.t;
  }

  type node = [ `Empty | `State of stateKey Data.key ]
  type arc = [ `Empty | `Arc of arcKey Data.key ]
  type attribute = P.attribute

  let sop p = `RoundedRectangle (p, 10.0, 0.75, 5.0)

  let arc_from_key graph k =
    let (), (arc, _, _) = Data.acca graph.arc k in
    !arc

  (* let get_node g i =
       match List.fold_right (fun n (j,v) ->
                 if j=i then (i+1,Some n)
                 else (j+1,v)) g.state (0,None) with
         _,None -> failwith "state does not exist"
       | _,Some x -> x

     let set_node g i vn =
       g.state <- snd @@ List.fold_right (fun ov (j,nl) ->
                             if j=i then (i+1,vn::nl)
                             else (j+1,ov::nl)) g.state (0,[])*)

  let new_graph () =
    { def = P.init_def (); state = Data.create (); arc = Data.create () }

  let remove_node graph = function
    | `Empty -> ()
    | `State p ->
        Data.iteri
          (fun k ((), (_, p1, p2)) ->
            if p = p1 || p = p2 then Data.remove graph.arc k)
          graph.arc;
        Data.remove graph.state p

  let get_new_node_attribute graph = function
    | `Empty ->
        let l = P.get_new_def_attr graph.def in
        List.map
          (fun (an, f) ->
            ( an,
              fun () ->
                let ns, id = f graph.def in
                graph.def <- ns;
                id ))
          l
    | `State n ->
        let _, (s, p) = Data.acca graph.state n in
        let l = P.get_new_state_attr !s !p in
        List.map
          (fun (an, f) ->
            ( an,
              fun () ->
                let ns, id = f !s in
                s := ns;
                id ))
          l

  let get_node_attribute graph = function
    | `Empty -> P.get_def_attr graph.def
    | `State n ->
        let _, (s, _) = Data.acca graph.state n in
        P.get_state_attr !s

  let update_node_attribute graph node attr_id newv =
    match node with
    | `Empty -> (
        match P.update_def_attr graph.def attr_id newv with
        | None -> false
        | Some d2 ->
            graph.def <- d2;
            true)
    | `State n -> (
        let _, (s, _) = Data.acca graph.state n in
        match P.update_state_attr !s attr_id newv with
        | None -> false
        | Some s2 ->
            s := s2;
            true)

  let move_node graph pos = function
    | `Empty -> ()
    | `State n ->
        let _, (_, p) = Data.acca graph.state n in
        p := pos

  let new_node graph _ ((a, b) as pos) =
    Printf.printf "new node: %f; %f\n" a b;
    let s = Data.addk ((), (ref (P.init_state ()), ref pos)) graph.state in
    `State s

  let get_new_node_choice _ = [ ((fun pos -> sop pos), 0) ]

  let shapes_of_node graph = function
    | `Empty -> []
    | `State n ->
        let _, (s, p) = Data.acca graph.state n in
        P.draw_state !s !p

  let iter_node graph f = Data.iteri (fun k _ -> f (`State k) 0) graph.state

  let shapes_of_arc graph pos1 pos2 = function
    | `Empty -> []
    | `Arc a -> P.draw_arc (arc_from_key graph a) (pos1, pos2)

  let remove_arc graph = function
    | `Empty -> ()
    | `Arc n -> Data.remove graph.arc n

  let get_arc_attribute graph = function
    | `Empty -> P.get_def_attr graph.def
    | `Arc k -> P.get_arc_attr (arc_from_key graph k)

  let update_arc_attribute graph arct attr_id attr_value =
    match arct with
    | `Empty -> update_node_attribute graph `Empty attr_id attr_value
    | `Arc k -> (
        let (), (arc, _, _) = Data.acca graph.arc k in
        match P.update_arc_attr !arc attr_id attr_value with
        | None -> false
        | Some newarc ->
            arc := newarc;
            true)

  let get_new_arc_attribute graph = function
    | `Empty -> get_new_node_attribute graph `Empty
    | `Arc k ->
        let _, (a, n1, n2) = Data.acca graph.arc k in
        let _, (_, p1) = Data.acca graph.state n1
        and _, (_, p2) = Data.acca graph.state n2 in
        let l = P.get_new_arc_attr !a (!p1, !p2) in
        List.map
          (fun (an, f) ->
            ( an,
              fun () ->
                let na, id = f !a in
                a := na;
                id ))
          l

  let new_arc graph n1 n2 =
    match (n1, n2) with
    | `State n1i, `State n2i -> (
        let _, (n1, _) = Data.acca graph.state n1i
        and _, (n2, _) = Data.acca graph.state n2i in
        match P.init_arc !n1 !n2 with
        | Some a -> Some (`Arc (Data.addk ((), (ref a, n1i, n2i)) graph.arc))
        | None -> None)
    | _ -> None

  let nodes_of_arc graph = function
    | `Empty -> (`Empty, `Empty)
    | `Arc k ->
        let _, (_, n1, n2) = Data.acca graph.arc k in
        (`State n1, `State n2)

  let iter_arc graph f = Data.iteri (fun k _ -> f (`Arc k)) graph.arc

  let print_graph =
    List.map
      (fun (name, pfun, file_name) ->
        ( name,
          (fun out graph ->
            pfun out graph.def
              (fun f ->
                Data.iteri
                  (fun k ((), (s, pos)) -> f (Data.unsafe_rev k) !s !pos)
                  graph.state)
              (fun f ->
                Data.iteri
                  (fun k ((), (s, source, target)) ->
                    let (), (sourcet, ps) = Data.acca graph.state source
                    and (), (targett, pt) = Data.acca graph.state target in
                    f (Data.unsafe_rev k) !s
                      ( (Data.unsafe_rev source, !sourcet, !ps),
                        (Data.unsafe_rev target, !targett, !pt) ))
                  graph.arc)),
          file_name ))
      P.print

  let read_graph s =
    let graph = new_graph () in
    P.parse_file s
      (fun node pos -> Data.addk ((), (ref node, ref pos)) graph.state)
      (fun arc n1 n2 -> Data.add ((), (ref arc, n1, n2)) graph.arc);
    graph
end
