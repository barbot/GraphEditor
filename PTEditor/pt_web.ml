open Js_of_ocaml
open GraphEditor.UtilsWeb

let _ = PrismType.allInt := true

module PTGraphEd = GraphEditor.GraphDrawing.Make (PTToGraph)

let _ =
  run' @@ fun () ->
  match Dom_html.(getElementById_coerce "pt_drawing" CoerceTo.canvas) with
  | Some canvas ->
      let ed =
        PTGraphEd.init
          ?slider:Dom_html.(getElementById_coerce "zoomslider" CoerceTo.input)
          ~saveload:Dom_html.(getElementById "saveload")
          canvas
          Dom_html.(getElementById "attr_list")
      in
      (*ed.graph <- PTGraphEd.parse_exchange_string "";*)
      ed.callback ed.graph;

      (*let txin = text_area ~class_:"input_file col-sm-6" ~on_change:(fun x->
                       file_content:=x                            ;
                       launch_compt false)  !file_content in
        launch_compt true;*)
      PTGraphEd.init_client ed
  | None -> failwith "no graph"
