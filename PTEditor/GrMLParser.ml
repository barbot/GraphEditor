open Xml
open PetriNet
open StochasticPetriNet
open Type

type attr =
  [ `DistrExpr of StochasticPetriNet.distr
  | `IntExpr of int Type.expr'
  | `FloatExpr of float Type.expr'
  | `BoolExpr of bool Type.expr'
  | `StringExpr of string
  | `Declaration of string * attr
  | `UnParsed of string ]

let rec printH_attr_val out = function
  | `StringExpr s -> Format.pp_print_text out s
  | `IntExpr i -> printH_expr out i
  | `FloatExpr f -> printH_expr out f
  | `BoolExpr b -> printH_expr out b
  | `DistrExpr d -> StochPTPrinter.printH_distr out d
  | `Declaration (d, a) -> Format.fprintf out "%s:%a" d printH_attr_val a
  | `UnParsed s -> Format.pp_print_text out s
  | `Choice (t :: _) -> Format.pp_print_text out t
  | `Choice [] -> failwith "empty choice"

(*   | `ControlPoint (f1,f2) -> Format.fprintf out "%f:%f" f1 f2
*)

let rec print_gen_attr_val out = function
  | `StringExpr s -> Format.pp_print_text out s
  | `IntExpr i ->
      Format.fprintf out "@[ <attribute name=\"expr\">%a</attribute>@]@,"
        StochPTPrinter.print_expr i
  | `FloatExpr f ->
      Format.fprintf out "@[ <attribute name=\"expr\">%a</attribute>@]@,"
        StochPTPrinter.print_expr f
  | `BoolExpr b ->
      Format.fprintf out "@[ <attribute name=\"expr\">%a</attribute>@]@,"
        StochPTPrinter.print_expr b
  | `DistrExpr d -> StochPTPrinter.print_distr out d
  | `Declaration (s, e) ->
      Format.fprintf out "@[ <attribute name=\"%s\">" s;
      print_gen_attr_val out e;
      Format.fprintf out "</attribute>@]@,"
  | `UnParsed s -> Format.pp_print_text out s

(* | `Choice l -> (match l with [] -> ()
                              |t::_ -> Format.pp_print_text out t)
   | `ControlPoint (f1,f2) -> Format.fprintf out "%f:%f" f1 f2
*)

type node_type = {
  mutable position : (float * float) option;
  mutable attr_list : (string * attr) list;
}

type gen_spt =
  (node_type, node_type, int Type.expr', (string * attr) list) Net.t

let print_gen_decl f = function
  | None -> ()
  | Some d ->
      Format.fprintf f "@[<hov 2> <attribute name=\"declaration\">@;";
      Format.fprintf f "@[<hov 2> <attribute name=\"constants\">@;";
      Format.fprintf f "@[<hov 2> <attribute name=\"intConsts\">@;";
      List.iter
        (function
          | s, `Declaration (n, v) when s = "intConst" ->
              Format.fprintf f
                "@[ <attribute name=\"intConst\">@,\
                 <attribute name=\"name\">%s</attribute>@,\
                 %a</attribute>@]@;"
                n print_gen_attr_val v
          | _ -> ())
        d;
      Format.fprintf f "</attribute>@]@;";
      Format.fprintf f "@[<hov 2> <attribute name=\"realConsts\">@;";
      List.iter
        (function
          | s, `Declaration (n, v) when s = "realConst" ->
              Format.fprintf f
                "@[ <attribute name=\"realConst\">@,\
                 <attribute name=\"name\">%s</attribute>@,\
                 %a</attribute>@]@;"
                n print_gen_attr_val v
          | _ -> ())
        d;
      Format.fprintf f "</attribute>@]@;";
      Format.fprintf f "</attribute>@]@;";

      List.iter
        (function
          | s, e when s <> "intConst" && s <> "realConst" ->
              Format.fprintf f
                "@[<hov 2> <attribute name=\"%s\">%a</attribute>@]@;" s
                print_gen_attr_val e
          | _ -> ())
        d;
      Format.fprintf f "</attribute>@]@;"

let print_gen_node node_type f _ id pl =
  let x, y = pl.position |>>| (0.0, 0.0) in
  Format.fprintf f
    "<node id=\"%i\" nodeType=\"%s\" x=\"%f\" y=\"%f\">@[<hov 2> @;" id
    node_type x y;
  List.iter
    (fun (a, v) ->
      Format.fprintf f "@[<hov 2> <attribute name=\"%s\">%a</attribute>@]@;" a
        print_gen_attr_val v)
    pl.attr_list;
  Format.fprintf f "@]</node>@;"

let print_gen_arc f id source target valuation inhib =
  let arctype = if inhib then "inhibitorarc" else "arc" in
  Format.fprintf f
    "@[<hov 2><arc id=\"%i\" arcType=\"%s\" source=\"%i\" target=\"%i\">@;" id
    arctype source target;
  Format.fprintf f
    "@[<hov 2><attribute name=\"valuation\"><attribute name=\"expr\">@,\
     @[%a@]@,\
     </attribute></attribute>@]@;\
     </arc>@]@;"
    StochPTPrinter.print_int_expr valuation

let tree_of_pnml s = parse_file s

let rec find_child s e1 = function
  | Element (_, _, [ PCData e2 ]) -> Some e2
  | Element (_, _, cl) -> List.fold_left (find_child s) e1 cl
  | PCData _ -> e1

let find_child_list s xl = List.fold_left (find_child s) None xl

let find_at s l = try Some (List.assoc s l) with Not_found -> None

let find_id l = find_at "id" l

let find_type l = find_at "nodeType" l

let rec find_name_rec s e = function
  | Element (x, _, cl) when s = x -> find_child_list "name" cl
  | Element (_, _, cl) -> List.fold_left (find_name_rec s) e cl
  | PCData _ -> e

let find_prop s l =
  List.fold_left
    (fun c n ->
      match n with
      | Element ("attribute", atl, cl) ->
          let v = find_at "name" atl in
          if v = Some s then Some cl else c
      | _ -> c)
    None l

let find_simp_prop s l =
  find_prop s l |>> function [ PCData x ] -> Some (String.trim x) | _ -> None

let find_name s t = find_name_rec s None t

let idmap = ref StringMap.empty

let rec parse_Grml_expr : type a. a expr' -> Xml.xml -> a expr' option =
 fun parseType e ->
  match e with
  | Element ("attribute", atl, cl) -> (
      let t = find_at "name" atl in
      match t with
      | Some "expr" -> opHd cl |>> parse_Grml_expr parseType
      | Some "numValue" | Some "intValue" -> (
          let sv =
            cl |> function [ PCData x ] -> Some (String.trim x) | _ -> None
          in
          match (sv, parseType) with
          | None, _ -> None
          | Some bv, Bool _ -> Some (Bool (bool_of_string bv))
          | Some iv, Int _ -> Some (Int (int_of_string iv))
          | Some fv, Float _ -> Some (Float (float_of_string fv))
          | _ -> None )
      | Some "name" -> (
          let sv =
            cl |> function [ PCData x ] -> Some (String.trim x) | _ -> None
          in
          match (sv, parseType) with
          | None, _ -> None
          | Some bv, Bool _ -> Some (BoolName bv)
          | Some iv, Int _ -> Some (IntName iv)
          | Some fv, Float _ -> Some (FloatName fv)
          | _ -> None )
      | Some te ->
          Printf.printf "Do not know what to do at type : %s\n" te;
          None
      | None -> None )
  | PCData x ->
      Printf.printf "Do not know what to do with : %s\n" x;
      None
  | _ ->
      Printf.printf "Ill formed GrMl\n";
      None

let int_expr_of_atr s at =
  let a =
    find_prop s at |>> function
    | x :: _ -> parse_Grml_expr (Int 0) x
    | _ -> None
  in
  a

let float_expr_of_atr s at =
  find_prop s at |>> function
  | x :: _ -> parse_Grml_expr (Float 0.0) x
  | _ -> None

let bool_expr_of_atr s at =
  find_prop s at |>> function
  | x :: _ -> parse_Grml_expr (Bool false) x
  | _ -> None

let parse_distr cl =
  let dist = find_simp_prop "type" cl in
  let larg =
    List.fold_left
      (fun c child ->
        match child with
        | Element ("attribute", [ ("name", "param") ], cl2) ->
            let n =
              Some (find_simp_prop "number" cl2 |>>> int_of_string |>>| 0)
            in
            let ex = float_expr_of_atr "expr" cl2 in
            Some n |>> (fun x -> ex |>>> fun y -> (x, y) :: c) |>>| c
        | _ -> c)
      [] cl
    |> List.sort (fun (i, _) (j, _) -> compare i j)
  in
  match (dist, larg) with
  | Some "EXPONENTIAL", [ (_, f) ] -> StochasticPetriNet.Exp f
  | Some "IMMEDIATE", [] -> Imm
  | Some "MASSACTION", [] -> StochasticPetriNet.Exp (Float 1.0)
  | Some "MASSACTION", [ (_, f) ] -> StochasticPetriNet.Exp f
  | Some "IMDT", [] -> Imm
  | Some "LOGNORMAL", [ (_, f1); (_, f2) ] -> LogNormal (f2, f1)
  | Some "DETERMINISTIC", [ (_, f) ] -> Det f
  | Some "UNIFORM", [ (_, f1); (_, f2) ] -> Unif (f2, f1)
  | Some "DISCRETEUNIF", [ (_, f1) ] -> DiscUnif (Floor f1)
  | Some "USERDEFINE", _ -> DiscUserDef 0
  | Some "PLAYER1", [] -> Player1
  | Some "ERLANG", [ (_, f1); (_, f2) ] -> Erl (Floor f2, f1)
  | Some "GEOMETRIC", [ (_, f1); (_, f2) ] -> Geometric (f2, f1)
  | Some "SYNC", [] -> Imm
  | Some x, y ->
      failwith
        ("Unknown distribution " ^ x ^ "/" ^ string_of_int (List.length y))
  | _ -> failwith "ill define distribution"

let parse_single_attr name v =
  match (name, v) with
  | "distribution", _ -> ("distribution", `DistrExpr (parse_distr v))
  | "name", [ PCData x ] -> ("name", `StringExpr (String.trim x))
  | "update", [ PCData x ] -> ("update", `StringExpr x)
  | "marking", [ a ] ->
      ("marking", `IntExpr (parse_Grml_expr (Int 0) a |>>| Int 0))
  | "weight", [ a ] ->
      ("weight", `FloatExpr (parse_Grml_expr (Float 1.0) a |>>| Float 1.0))
  | "priority", [ a ] ->
      ("priority", `FloatExpr (parse_Grml_expr (Float 1.0) a |>>| Float 1.0))
  | "service", [ a ] ->
      ("service", `IntExpr (parse_Grml_expr (Int 1) a |>>| Int 1))
  | "domain", _ ->
      ( "domain",
        `UnParsed (List.fold_left (fun s v2 -> s ^ Xml.to_string v2) "" v) )
  | "guard", _ ->
      ("guard", `BoolExpr (bool_expr_of_atr "guard" v |>>| Bool true))
  | _ ->
      Printf.printf "Fail to parse attribute:'%s' keep unparsed \n" name;
      (name, `UnParsed (List.fold_left (fun s v2 -> s ^ Xml.to_string v2) "" v))

let parse_attr at =
  List.map
    (function
      | Element ("attribute", atl, cl) -> (
          match find_at "name" atl with
          | Some n -> parse_single_attr n cl
          | _ -> failwith "ill define GrMl" )
      | _ -> failwith "ill define GrMl")
    at

let rec net_of_tree (n : gen_spt) = function
  | Element (name, alist, clist) -> (
      (*Printf.printf "new xmlnode: %s\n" name;*)
      match name with
      | "xml" | "model" -> List.iter (net_of_tree n) clist
      | "attribute" -> (
          match find_at "name" alist |>>| "" with
          | "declaration" | "constants" | "intConsts" | "realConsts" ->
              List.iter (net_of_tree n) clist
          | "intConst" -> (
              let nameopt = find_simp_prop "name" clist in
              let value = int_expr_of_atr "expr" clist in
              match (nameopt, n.Net.def) with
              | Some name, Some def ->
                  Format.fprintf !logout "intconst: %s:%a\n" name printH_expr
                    (value |>>| Int 0);
                  n.Net.def <-
                    Some
                      ( ( "intConst",
                          `Declaration (name, `IntExpr (value |>>| Int 0)) )
                      :: def )
              | _, None -> failwith "undefined preembule"
              | _ -> failwith "fail to read intConst" )
          | "realConst" -> (
              let nameopt = find_simp_prop "name" clist in
              let value = float_expr_of_atr "expr" clist in
              match (nameopt, n.Net.def) with
              | Some name, Some def ->
                  Format.fprintf !logout "realConst: %s:%a\n" name printH_expr
                    (value |>>| Float 0.0);
                  n.Net.def <-
                    Some
                      ( ( "realConst",
                          `Declaration (name, `FloatExpr (value |>>| Float 0.0))
                        )
                      :: def )
              | _, None -> failwith "undefined preembule"
              | _ -> failwith "fail to read intConst" )
          | x ->
              let v =
                List.fold_left (fun s x -> s ^ Xml.to_string x) "" clist
              in
              n.Net.def <- Some ((x, `UnParsed v) :: (n.Net.def |>>| []));
              Printf.fprintf stderr
                "Dont know what how to parse \"%s\" keep as it is \n" x )
      | "node" when find_at "nodeType" alist = Some "transition" -> (
          match (find_simp_prop "name" clist, find_id alist) with
          | Some name, Some id ->
              let position =
                match
                  ( find_at "x" alist |>>> float_of_string,
                    find_at "y" alist |>>> float_of_string )
                with
                | Some x, Some y -> Some (x, y)
                | _ -> None
              in
              idmap := StringMap.add id name !idmap;
              let attr_list = parse_attr clist in

              Data.add (name, { attr_list; position }) n.Net.transition
          | _ -> () )
      | "node" when find_at "nodeType" alist = Some "place" -> (
          match (find_simp_prop "name" clist, find_id alist) with
          | Some name, Some id ->
              let position =
                match
                  ( find_at "x" alist |>>> float_of_string,
                    find_at "y" alist |>>> float_of_string )
                with
                | Some x, Some y -> Some (x, y)
                | _ -> None
              in
              idmap := StringMap.add id name !idmap;
              let attr_list = parse_attr clist in

              Data.add (name, { attr_list; position }) n.Net.place
          | _ -> () )
      (* |  "node" when find_at "nodeType" alist = Some "place" ->
           let bo = (int_expr_of_atr "bound" clist) in
                  let position = match find_at "x" alist |>>> float_of_string,
                                       find_at "y" alist |>>> float_of_string with
                      Some x,Some y -> Some (x,y) |_ -> None in
           ( match ((find_simp_prop "name" clist),(int_expr_of_atr "marking" clist),(find_id alist)) with
                  | (Some name,Some im,Some id) ->  begin
           idmap := StringMap.add id name !idmap;
             	   Format.fprintf !logout "new place: %s\n\tmarking: %a\n" name printH_expr im;
           Data.add (name, {marking=im; bound=bo; positionP=position}) n.Net.place;
         end
                | (Some name,None,Some id) -> begin
           Format.fprintf !logout "new place: %s\n" name;
           idmap := StringMap.add id name !idmap;
           Data.add (name,{marking=Int 0; bound=bo; positionP=position}) n.Net.place;
         end
                | (None,None,Some _) ->  Printf.fprintf stderr "Unknown node\n";
         | _ ->()

                  )*)
      | "arc" -> (
          let sid = find_at "source" alist |>>> fun x -> StringMap.find x !idmap
          and tid = find_at "target" alist |>>> fun x -> StringMap.find x !idmap
          and arctype = find_at "arcType" alist |>>| "arc" in
          match
            ( find_id alist,
              sid,
              tid,
              int_expr_of_atr "valuation" clist,
              arctype = "inhibitorarc" )
          with
          | _, Some source, Some target, Some v, false ->
              Format.fprintf !logout " (%s)-(%a)->(%s)\n" source printH_expr v
                target;
              Net.add_arc n source target v
          | _, Some source, Some target, Some v, true ->
              Format.fprintf !logout " (%s)-(%a)-o(%s)\n" source printH_expr v
                target;
              Net.add_inhibArcS n source target v
          | _, Some source, Some target, None, false ->
              Printf.printf " (%s)-(1)->(%s)\n" source target;
              Net.add_arc n source target (Int 1)
          | _, Some source, Some target, None, true ->
              Printf.printf " (%s)-(1)-o(%s)\n" source target;
              Net.add_inhibArcS n source target (Int 1)
          | _ -> () )
      | x -> Printf.printf "Dont know what to do with %s, ignore\n" x )
  | _ -> ()

let genPT_of_tree tree =
  let net = PetriNet.Net.create () in
  net.PetriNet.Net.def <- Some [];
  net_of_tree net tree;
  net

let spt_form_genPT _ =
  let net = PetriNet.Net.create () in
  let open StochasticPetriNet in
  net.PetriNet.Net.def <-
    Some
      {
        intconst = [];
        floatconst = [];
        clock = [];
        labels = [];
        formulas = [];
        classDef = [];
        printer = (fun _ _ -> ());
      };
  net
