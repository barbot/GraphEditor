module Point : sig
  type t = float * float

  val print_point : out_channel -> t -> unit
  val ( +.. ) : t -> t -> t
  val ( -.. ) : t -> t -> t
  val ( ~-.. ) : t -> t
  val ( *.. ) : t -> t -> float
  val mult : float -> t -> t
  val vect_prod : t -> t -> t
  val vect_div : t -> t -> t
  val vect_sqrt : t -> t
  val norm : t -> float
  val dist : t -> float
  val abs_point : t -> t
  val gaussian : float -> t -> float
  val proj : t -> t -> t
  val rot : float -> t -> t
  val pi : float
  val normal : t -> t
  val angle : t -> float
end

type point = Point.t

module Color : sig
  type t = int * int * int

  val mix : 'a * 'b * 'c -> 'a * 'b * 'c -> 'a * 'b * 'c
  val parse : string -> int * int * int
  val to_string : int * int * int -> string
  val to_tikz_string : int * int * int -> string
end

module Canvas : sig
  type t = {
    mutable strokeColor : Color.t;
    mutable fillColor : Color.t;
    mutable ambiant : Color.t;
  }

  val canvasState : t

  val syncColor :
    Js_of_ocaml.Dom_html.canvasRenderingContext2D Js_of_ocaml.Js.t -> unit

  val setStrokeColor :
    Js_of_ocaml.Dom_html.canvasRenderingContext2D Js_of_ocaml.Js.t ->
    Color.t ->
    unit

  val setFillColor :
    Js_of_ocaml.Dom_html.canvasRenderingContext2D Js_of_ocaml.Js.t ->
    Color.t ->
    unit

  val setAmbiant :
    Js_of_ocaml.Dom_html.canvasRenderingContext2D Js_of_ocaml.Js.t ->
    Color.t ->
    unit

  val flipColor :
    Js_of_ocaml.Dom_html.canvasRenderingContext2D Js_of_ocaml.Js.t -> unit
end

module Circle : sig
  type t = point * float

  val is_over : (point -> point) -> point -> t -> bool

  val draw :
    (point -> point) ->
    Js_of_ocaml.Dom_html.canvasRenderingContext2D Js_of_ocaml.Js.t ->
    ?thick:float ->
    t ->
    unit

  val projection : t -> point -> point
end

module Rectangle : sig
  type t = point * float * float * float

  val of_border : point -> point -> t
  val is_over : (point -> point) -> point -> t -> bool
  val contain : t -> point -> bool
  val contains : t -> (point * 'b) list -> 'b list

  val draw :
    (point -> point) ->
    Js_of_ocaml.Dom_html.canvasRenderingContext2D Js_of_ocaml.Js.t ->
    ?thick:float ->
    ?fill:bool ->
    t ->
    unit

  val projection : t -> point -> point
end

module RoundedRectangle : sig
  type t = point * float * float * float

  val draw :
    (point -> point) ->
    Js_of_ocaml.Dom_html.canvasRenderingContext2D Js_of_ocaml.Js.t ->
    ?thick:float ->
    t ->
    unit

  val is_over : (point -> point) -> point -> t -> bool
  val projection : t -> point -> point
end

module Text : sig
  type t = point * string

  val bounding_box :
    (point -> point) ->
    Js_of_ocaml.Dom_html.canvasRenderingContext2D Js_of_ocaml.Js.t ->
    t ->
    point * float * float * float

  val draw :
    (point -> point) ->
    Js_of_ocaml.Dom_html.canvasRenderingContext2D Js_of_ocaml.Js.t ->
    ?thick:float ->
    t ->
    unit
end

module TokenSet : sig
  type t = point * float * int

  val draw :
    (point -> point) ->
    Js_of_ocaml.Dom_html.canvasRenderingContext2D Js_of_ocaml.Js.t ->
    ?thick:float ->
    t ->
    unit
end

module Line : sig
  type t = point * point

  val draw :
    (point -> point) ->
    Js_of_ocaml.Dom_html.canvasRenderingContext2D Js_of_ocaml.Js.t ->
    thick:float ->
    t ->
    unit

  val is_over : (point -> point) -> point -> thick:float -> t -> bool
  val point_at : float -> ?offset:float -> t -> point
end

module Bezier2 : sig
  type t = point * point * point

  val draw :
    (point -> point) ->
    Js_of_ocaml.Dom_html.canvasRenderingContext2D Js_of_ocaml.Js.t ->
    thick:float ->
    t ->
    unit

  val solve_linear : float -> float -> float list
  val solve_quadratic : float -> float -> float -> float list
  val poly_of : (point -> point) -> t -> point * point * point
  val is_over : (point -> point) -> point -> thick:float -> t -> bool
  val point_at : float -> ?offset:float -> t -> point
end

module Bezier3 : sig
  type t = point * point * point * point

  val draw :
    (point -> point) ->
    Js_of_ocaml.Dom_html.canvasRenderingContext2D Js_of_ocaml.Js.t ->
    thick:float ->
    t ->
    unit

  val solve_cubic : float -> float -> float -> float -> float list
  val poly_of : (point -> point) -> t -> point * point * point * point
  val is_over : (point -> point) -> point -> thick:float -> t -> bool
  val point_at : float -> ?offset:float -> t -> point
end

module Arrow : sig
  type t = point * point

  val draw :
    (point -> point) ->
    Js_of_ocaml.Dom_html.canvasRenderingContext2D Js_of_ocaml.Js.t ->
    thick:float ->
    t ->
    unit
end

module SimpleArrow : sig
  type t = point * point

  val draw :
    (point -> point) ->
    Js_of_ocaml.Dom_html.canvasRenderingContext2D Js_of_ocaml.Js.t ->
    thick:float ->
    t ->
    unit
end

module RoundArrow : sig
  type t = point * point

  val draw :
    (point -> point) ->
    Js_of_ocaml.Dom_html.canvasRenderingContext2D Js_of_ocaml.Js.t ->
    thick:float ->
    t ->
    unit
end

type shape =
  [ `Arrow of Arrow.t
  | `Bezier2 of Bezier2.t
  | `Bezier3 of Bezier3.t
  | `Circle of Circle.t
  | `Colors of Color.t * Color.t
  | `Empty
  | `Line of Line.t
  | `Rectangle of Rectangle.t
  | `RoundArrow of RoundArrow.t
  | `RoundedRectangle of RoundedRectangle.t
  | `SimpleArrow of SimpleArrow.t
  | `Text of Text.t
  | `TokenSet of TokenSet.t ]

type path =
  [ `Arrow of Arrow.t
  | `Bezier2 of Bezier2.t
  | `Bezier3 of Bezier3.t
  | `Colors of Color.t * Color.t
  | `Empty
  | `Line of Line.t
  | `RoundArrow of RoundArrow.t
  | `SimpleArrow of SimpleArrow.t ]

val center_shape : shape -> point

val tangible :
  [> `Circle of 'a | `Rectangle of 'b | `RoundedRectangle of 'c | `Text of 'd ]
  list ->
  [> `Circle of 'a
  | `Empty
  | `Rectangle of 'b
  | `RoundedRectangle of 'c
  | `Text of 'd ]

val center_shapes : shape list -> point
val projection_shape : point -> shape -> point

val draw_shape :
  (point -> point) ->
  Js_of_ocaml.Dom_html.canvasRenderingContext2D Js_of_ocaml.Js.t ->
  ?thick:float ->
  shape ->
  unit

val print_shape :
  out_channel ->
  [< `Arrow of Arrow.t | `Bezier2 of Bezier2.t | `Empty | `Line of Line.t ] ->
  unit

val draw_shapes :
  (point -> point) ->
  Js_of_ocaml.Dom_html.canvasRenderingContext2D Js_of_ocaml.Js.t ->
  ?thick:float ->
  shape list ->
  unit

val is_over_shape : (point -> point) -> point -> shape -> bool
val is_over_shapes : (point -> point) -> point -> shape list -> bool

type path_elem =
  [ `ControlPoint of point | `Point of point | `Text of float * float * string ]

val point_of_path_elem : [< `ControlPoint of 'a | `Point of 'a ] -> 'a
val project_path : shape -> point list -> shape -> point * point list * point

val shapes_of_path :
  shape ->
  ?arrow1:(point -> point -> shape) ->
  path_elem list ->
  ?arrow2:(point -> point -> shape) ->
  shape ->
  shape list
