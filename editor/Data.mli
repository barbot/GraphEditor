exception Empty
exception Destroyed

type 'a key
type ('a, 'b, 'k) t

val create : unit -> ('a, 'b, 'k) t
val acca : ('a, 'b, 'k) t -> 'k key -> 'a * 'b
val index : ('a, 'b, 'k) t -> 'a -> 'k key
val acc : ('a, 'b, 'k) t -> 'a -> 'b
val updatea : 'k key -> 'b -> ('a, 'b, 'k) t -> unit
val add : 'a * 'b -> ('a, 'b, 'k) t -> unit
val addk : 'a * 'b -> ('a, 'b, 'k) t -> 'k key
val remove : ('a, 'b, 'k) t -> 'k key -> unit
val foldi : ('a -> 'k key -> 'b * 'c -> 'a) -> 'a -> ('b, 'c, 'k) t -> 'a
val fold : ('a -> 'b * 'c -> 'a) -> 'a -> ('b, 'c, 'k) t -> 'a

val reduce :
  ('a * 'b -> 'a * 'b -> 'a * 'b) -> ('a, 'b, 'k) t -> ('a * 'b) option

val iteri : ('k key -> 'a * 'b -> unit) -> ('a, 'b, 'k) t -> unit
val iter : ('a * 'b -> unit) -> ('a, 'b, 'k) t -> unit
val map : ('a -> 'b -> 'a * 'c) -> ('a, 'b, 'k) t -> ('a, 'c, 'k) t
val filter : ('a -> 'b -> bool) -> ('a, 'b, 'k) t -> ('a, 'b, 'k) t
val adds : ('a, 'b, 'k) t -> ('a, 'b, 'l) t -> unit
val size : ('a, 'b, 'k) t -> int
val sample : ('a, 'b, 'k) t -> 'a * 'b
val copy : ('a, 'b, 'k) t -> ('a, 'b, 'l) t
val unsafe : int -> 'a key
val unsafe_rev : 'a key -> int
