open Js_of_ocaml

let to_js_float (x,y) =
  (Js.float x,Js.float y) 

module Point = struct
  type t = float * float

  let print_point f (x, y) = Printf.fprintf f "(%f,%f)" x y
  let ( +.. ) (x1, y1) (x2, y2) = (x2 +. x1, y2 +. y1)
  let ( -.. ) (x2, y2) (x1, y1) = (x2 -. x1, y2 -. y1)
  let ( ~-.. ) (x, y) = (-.x, -.y)
  let ( *.. ) (x1, y1) (x2, y2) = (x1 *. x2) +. (y1 *. y2)
  let mult s (x1, y1) = (s *. x1, s *. y1)
  let vect_prod (x1, y1) (x2, y2) = (x1 *. x2, y1 *. y2)
  let vect_div (x1, y1) (x2, y2) = (x1 /. x2, y1 /. y2)
  let vect_sqrt (x, y) = (sqrt x, sqrt y)
  let norm (x, y) = (x *. x) +. (y *. y)
  let dist p = sqrt (norm p)
  let abs_point (x, y) = (abs_float x, abs_float y)
  let gaussian k (x, y) = exp (-.((x *. x) +. (y *. y)) /. (k *. k))
  let proj x y = mult (y *.. x /. norm y) y

  let rot rho (x, y) =
    ((x *. cos rho) -. (y *. sin rho), (x *. sin rho) +. (y *. cos rho))

  let pi = 4.0 *. atan 1.0
  let normal (x, y) = mult (1.0 /. dist (y, -.x)) (y, -.x)

  let angle (x, y) =
    if x > 0.0 then atan (y /. x)
    else if x < 0.0 then pi +. atan (y /. x)
    else if y > 0.0 then pi /. 2.0
    else -.pi /. 2.0
end

type point = Point.t

open Point

module Color = struct
  type t = int * int * int

  let mix (r1, g1, b1) (r2, g2, b2) = (max r1 r2, max g1 g2, max b1 b2)

  let parse = function
    | "black" -> (0, 0, 0)
    | "red" -> (255, 0, 0)
    | "green" -> (0, 255, 0)
    | "blue" -> (0, 0, 255)
    | "white" -> (255, 255, 255)
    | "brown" -> (165, 42, 42)
    | "orange" -> (255, 165, 0)
    | "cyan" -> (0, 255, 255)
    | "fuchsia" -> (255, 0, 255)
    | "yellow" -> (255, 255, 0)
    | "darkgray" -> (169, 169, 169)
    | "gray" -> (128, 128, 128)
    | "lightgray" -> (211, 211, 211)
    | x when String.length x >= 7 && x.[0] = '#' -> (
        try
          let int_of_l = function
            | '0' -> 0
            | '1' -> 1
            | '2' -> 2
            | '3' -> 3
            | '4' -> 4
            | '5' -> 5
            | '6' -> 6
            | '7' -> 7
            | '8' -> 8
            | '9' -> 9
            | 'a' -> 10
            | 'b' -> 11
            | 'c' -> 12
            | 'd' -> 13
            | 'e' -> 14
            | 'f' -> 15
            | _ -> 0
          in
          let ios a b = (int_of_l a * 16) + int_of_l b in
          (ios x.[1] x.[2], ios x.[3] x.[4], ios x.[5] x.[6])
        with _ -> (0, 0, 0))
    | x when String.length x >= 4 && x.[0] = 'r' && x.[1] = 'g' && x.[2] = 'b'
      ->
        Scanf.sscanf x "rgb,255:red,%i;green,%i;blue,%i" (fun r g b ->
            (r, g, b))
    | _ -> (0, 0, 0)

  let to_string (r, g, b) =
    let loi = function
      | 0 -> '0'
      | 1 -> '1'
      | 2 -> '2'
      | 3 -> '3'
      | 4 -> '4'
      | 5 -> '5'
      | 6 -> '6'
      | 7 -> '7'
      | 8 -> '8'
      | 9 -> '9'
      | 10 -> 'a'
      | 11 -> 'b'
      | 12 -> 'c'
      | 13 -> 'd'
      | 14 -> 'e'
      | 15 -> 'f'
      | _ -> '0'
    in
    Printf.sprintf "#%c%c%c%c%c%c"
      (loi (r / 16))
      (loi (r mod 16))
      (loi (g / 16))
      (loi (g mod 16))
      (loi (b / 16))
      (loi (b mod 16))

  let to_tikz_string (r, g, b) =
    Printf.sprintf "rgb,255:red,%i;green,%i;blue,%i" r g b
end

module Canvas = struct
  type t = {
    mutable strokeColor : Color.t;
    mutable fillColor : Color.t;
    mutable ambiant : Color.t;
  }

  let canvasState =
    { strokeColor = (0, 0, 0); fillColor = (0, 0, 0); ambiant = (0, 0, 0) }

  let syncColor ctx =
    let rs, gs, bs = Color.mix canvasState.strokeColor canvasState.ambiant
    and rf, gf, bf = Color.mix canvasState.fillColor canvasState.ambiant in
    ctx##.strokeStyle := Js.string (Printf.sprintf "rgb(%i,%i,%i)" rs gs bs);
    ctx##.fillStyle := Js.string (Printf.sprintf "rgb(%i,%i,%i)" rf gf bf)

  let setStrokeColor ctx c =
    canvasState.strokeColor <- c;
    syncColor ctx

  let setFillColor ctx c =
    canvasState.fillColor <- c;
    syncColor ctx

  let setAmbiant ctx c =
    canvasState.ambiant <- c;
    syncColor ctx

  let flipColor ctx =
    let sc = canvasState.strokeColor and fc = canvasState.fillColor in
    setStrokeColor ctx fc;
    setFillColor ctx sc
end

module Circle = struct
  type t = point * float

  let is_over to_screen mouse_pos (center, radius) =
    let x, y = to_screen center
    and r, _ = to_screen (center +.. (radius, 0.0)) in
    dist (mouse_pos -.. (x, y)) <= r -. x

  let draw to_screen ctx ?(thick = 3.0) (center, radius) =
    let x, y = to_screen center
    and r, _ = to_screen (center +.. (radius, 0.0)) in
    let width = r -. x in
    ctx##.lineWidth := Js.float thick;
    ctx##beginPath;
    ctx ## (arc (Js.float x) (Js.float y) (Js.float width) (Js.float 0.0) (Js.float 6.28318530717958) (Js.bool true));
    ctx##fill;
    ctx##stroke

  let projection (center, radius) origin =
    let v1 = center -.. origin in
    let nd = dist v1 in
    origin +.. mult ((nd -. radius) /. nd) v1
end

module Rectangle = struct
  type t = point * float * float * float

  let of_border pos1 pos2 =
    let center = mult 0.5 (pos2 +.. pos1) in
    let diagx, diagy =
      match pos2 -.. pos1 with x, y -> (abs_float x, abs_float y)
    in
    (center, diagy /. diagx, diagy /. 2.0, 0.0)

  let is_over to_screen mouse_pos (center, asym, radius, angle) =
    let x, y = to_screen center
    and w, h = to_screen (center +.. (radius /. asym, radius)) in
    let width = w -. x and height = h -. y in
    let dmx, dmy = rot (-.angle) (mouse_pos -.. (x, y)) in
    abs_float dmx <= width && abs_float dmy <= height

  let contain ((x, y), asym, radius, _) (x2, y2) =
    let w, h = (radius /. asym, radius) in
    x2 >= x -. w && x2 <= x +. w && y2 >= y -. h && y2 <= y +. h

  let contains ((x, y), asym, radius, _) ptlist =
    let w, h = (radius /. asym, radius) in
    List.filter
      (fun ((x2, y2), _) ->
        x2 >= x -. w && x2 <= x +. w && y2 >= y -. h && y2 <= y +. h)
      ptlist
    |> List.split |> snd

  let draw to_screen ctx ?(thick = 3.0) ?(fill = true)
      (center, asym, radius, angle) =
    let x, y = to_screen center
    and w, h = to_screen (center +.. (radius /. asym, radius)) in
    let width = w -. x and height = h -. y in
    ctx##.lineWidth := Js.float thick;
    ctx##beginPath;
    let r p = let a,b = (x, y) +.. rot angle p in Js.float a, Js.float b in
    let x1, y1 = r (-.width, -.height) in
    ctx ## (moveTo x1 y1);
    let x2, y2 = r (+.width, -.height) in
    ctx ## (lineTo x2 y2);
    let x3, y3 = r (+.width, +.height) in
    ctx ## (lineTo x3 y3);
    let x4, y4 = r (-.width, +.height) in
    ctx ## (lineTo x4 y4);
    ctx ## (lineTo x1 y1);
    if fill then ctx##fill;
    ctx##stroke;
    ()

  let projection (center, asym, radius, angle) origin =
    let ((x1, y1) as v1) = rot (-.angle) (center -.. origin) in
    let intersect =
      mult ((abs_float x1 -. (radius /. asym)) /. abs_float x1) v1
    in
    let _, y2 = v1 -.. intersect in
    if abs_float y2 < radius then origin +.. rot angle intersect
    else
      let intersect2 = mult ((abs_float y1 -. radius) /. abs_float y1) v1 in
      origin +.. rot angle intersect2
end

module RoundedRectangle = struct
  type t = point * float * float * float

  let draw to_screen ctx ?(thick = 3.0) (center, radius, asym, rad) =
    let x, y = to_screen center in
    let w, h = to_screen (center +.. (radius /. asym, radius))
    and r, _ = to_screen (center +.. (rad, 0.0)) -.. (x, y) in
    let width = w -. x and height = h -. y in
    ctx##.lineWidth := Js.float thick;
    let mv x y = ctx##moveTo (Js.float x) (Js.float y) in
    let lineTo x y = ctx##lineTo (Js.float x) (Js.float y) in
    let arcTo x y w h r = ctx##arcTo (Js.float x) (Js.float y) (Js.float w) (Js.float h)  (Js.float r) in
    ctx##beginPath;
    mv (x -. width) (y -. height +. r);
    arcTo (x -. width) (y -. height) (x -. width +. r) (y -. height) r;
    lineTo (x +. width -. r) (y -. height);
    arcTo (x +. width) (y -. height) (x +. width) (y -. height +. r) r;
    lineTo (x +. width) (y +. height -. r);
    arcTo (x +. width) (y +. height) (x +. width -. r) (y +. height) r;
    lineTo (x -. width +. r) (y +. height);
    arcTo (x -. width) (y +. height) (x -. width) (y +. height -. r) r;
    lineTo (x -. width) (y -. height +. r);
    ctx##fill;
    ctx##stroke

  let is_over to_screen mouse_pos (center, radius, asym, rad) =
    let x, y = to_screen center in
    let w, h = to_screen (center +.. (radius /. asym, radius)) -.. (x, y)
    and r, _ = to_screen (center +.. (rad, 0.0)) -.. (x, y) in
    let dmx, dmy = mouse_pos -.. (x, y) in
    let dmx2, dmy2 = (abs_float dmx, abs_float dmy) in
    (dmx2 <= w -. r && dmy2 <= h)
    || (dmx2 <= w && dmy2 <= h -. r)
    || dist (dmx2 -. w +. r, dmy2 -. h +. r) <= r +. 2.0

  let projection (center, radius, asym, rad) origin =
    let ((x1, y1) as v1) = center -.. origin in
    let intersect = mult ((abs_float x1 -. (radius /. asym)) /. abs_float x1) v1
    and intersect2 = mult ((abs_float y1 -. radius) /. abs_float y1) v1 in
    let _, y2 = v1 -.. intersect in
    let x2, _ = v1 -.. intersect2 in
    let vect =
      if abs_float y2 < radius -. rad then origin +.. intersect
      else if abs_float x2 < (radius /. asym) -. rad then origin +.. intersect2
      else
        let x, y = center in
        let innercorner =
          ( (if x1 < 0.0 then x +. (radius /. asym) -. rad
            else x -. (radius /. asym) +. rad),
            if y1 < 0.0 then y +. radius -. rad else y -. radius +. rad )
        in
        let v4 = innercorner -.. center in
        let alpha = angle v4 -. angle (origin -.. center) in
        let rR = dist v4 in
        let nd2 =
          rR
          *. (cos alpha
             +. sqrt ((rad *. rad /. (rR *. rR)) -. (sin alpha *. sin alpha)))
        in

        let nd = dist v1 in
        origin +.. mult ((nd -. nd2) /. nd) v1
    in
    vect
end

module Text = struct
  type t = point * string

  let bounding_box to_screen ctx (center, text) =
    let c2 = to_screen center in
    if text = "" then (c2, 1.0, 0.0, 0.0)
    else
      let measure = ctx##measureText (Js.string text) in
      let w = (Js.to_float measure##.width) *. 0.4 in
      let h = 15.0 *. 0.4 in
      (c2, h /. w, h, 0.0)

  let draw to_screen ctx ?thick:_ (center, text) =
    let x, y = to_js_float @@ to_screen center in
    Canvas.flipColor ctx;
    (*Rectangle.draw (fun x -> x) ctx ~fill:true (bounding_box to_screen ctx (center,text));*)
    ctx##.textAlign := Js.string "center";
    ctx##.textBaseline := Js.string "middle";
    ctx##fillText (Js.string text) x y;
    Canvas.flipColor ctx
end

module TokenSet = struct
  type t = point * float * int

  let draw to_screen ctx ?(thick = 2.5) (center, radius, n) =
    let x, y = to_screen center in
    let r, _ = to_screen (center +.. (radius, 0.0)) -.. (x, y) in
    let th, _ = to_screen (center +.. (thick, 0.0)) -.. (x, y) in
    let width =
      match n with
      | 1 -> 0.0
      | 2 -> 0.75 *. r
      | 3 -> 0.80 *. r
      | 4 -> 0.90 *. r
      | _ -> r
    in
    for i = 0 to n - 1 do
      let rho = 2.0 *. pi *. float i /. float n in
      let x2, y2 = (x, y) +.. rot rho (width, 0.0) in
      ctx##beginPath;
      ctx ## (arc (Js.float x2) (Js.float y2) (Js.float th) (Js.float 0.0) (Js.float (2. *. pi)) (Js.bool true));
      ctx##fill
    done
end

module Line = struct
  type t = point * point

  let draw to_screen ctx ~thick (spos, epos) =
    let x1, y1 = to_screen spos and x2, y2 = to_screen epos in
    ctx##.lineWidth := Js.float thick;
    ctx##beginPath;
    ctx ## (moveTo (Js.float x1) (Js.float y1));
    ctx ## (lineTo (Js.float x2) (Js.float y2));
    ctx##stroke

  let is_over tos mouse_pos ~thick (spos, epos) =
    let sspos = tos spos and sepos = tos epos in
    let line = sepos -.. sspos and mouse_v = mouse_pos -.. sspos in
    let scal = line *.. (mouse_pos -.. sspos) /. norm line in
    let yv = mouse_v -.. mult scal line in
    norm yv < thick && scal > 0.0 && scal < 1.0

  let point_at t ?(offset = 0.0) (spos, epos) =
    let p0 = spos and p1 = epos in
    let no = normal (p1 -.. p0) in
    p0 +.. mult t (p1 -.. p0) +.. mult offset no
end

module Bezier2 = struct
  type t = point * point * point

  let draw to_screen ctx ~thick (spos, control, epos) =
    let x1, y1 = to_screen spos
    and xc, yc = to_screen control
    and x2, y2 = to_screen epos in
    ctx##.lineWidth := Js.float thick;
    ctx##beginPath;
    ctx ## (moveTo (Js.float x1) (Js.float y1));
    ctx ## (quadraticCurveTo (Js.float xc) (Js.float yc) (Js.float x2) (Js.float y2));
    ctx##stroke

  let poly_of tos (spos, control, epos) =
    let p0 = tos spos and p1 = tos control and p2 = tos epos in
    (p2 -.. mult 2.0 p1 +.. p0, mult 2.0 p1 -.. mult 2.0 p0, p0)

  let solve_linear a b = if a = 0.0 then [] else [ -.b /. a ]

  let solve_quadratic a b c =
    if a = 0.0 then solve_linear b c
    else
      let d = (b *. b) -. (4.0 *. a *. c) in
      if d > 0.0 then
        let t1 = (sqrt d -. b) /. (2.0 *. a)
        and t2 = (-.sqrt d -. b) /. (2.0 *. a) in
        [ t1; t2 ]
      else if d = 0.0 then [ -.b /. (2.0 *. a) ]
      else []

  let is_over tos mouse_pos ~thick bez =
    let (xA, yA), (xB, yB), c = poly_of tos bez in
    let xC, yC = c -.. mouse_pos in
    let epsx t =
      let x1 = (xA *. t *. t) +. (xB *. t) +. xC in
      t >= 0.0 && t <= 1.0 && abs_float x1 <= 1.5 *. thick
    in
    let epsy t =
      let y1 = (yA *. t *. t) +. (yB *. t) +. yC in
      t >= 0.0 && t <= 1.0 && abs_float y1 <= 1.5 *. thick
    in
    let b =
      List.exists epsy (solve_quadratic xA xB xC)
      || List.exists epsx (solve_quadratic yA yB yC)
    in

    b

  let point_at t ?(offset = 0.0) bez =
    let a, b, c = poly_of (fun x -> x) bez in
    let no = normal @@ (mult (2.0 *. t) a +.. b) in
    mult (t *. t) a +.. mult t b +.. c +.. mult offset no
end

module Bezier3 = struct
  type t = point * point * point * point

  let draw to_screen ctx ~thick (spos, control1, control2, epos) =
    let x1, y1 = to_screen spos
    and xc1, yc1 = to_screen control1
    and xc2, yc2 = to_screen control2
    and x2, y2 = to_screen epos in
    ctx##.lineWidth := Js.float thick;
    ctx##beginPath;
    ctx ## (moveTo (Js.float x1) (Js.float y1));
    ctx ## (bezierCurveTo (Js.float xc1) (Js.float yc1) (Js.float xc2) (Js.float yc2) (Js.float x2) (Js.float y2));
    ctx##stroke

  let poly_of tos (spos, control1, control2, epos) =
    let p0 = tos spos
    and p1 = tos control1
    and p2 = tos control2
    and p3 = tos epos in
    let a = p3 -.. mult 3.0 p2 +.. mult 3.0 p1 -.. p0 in
    let b = mult 3.0 p2 -.. mult 6.0 p1 +.. mult 3.0 p0
    and c = mult 3.0 p1 -.. mult 3.0 p0
    and d = p0 in
    (a, b, c, d)

  let solve_cubic a1 b1 c1 d1 =
    if a1 = 0.0 then Bezier2.solve_quadratic b1 c1 d1
    else
      let a = b1 /. a1 and b = c1 /. a1 and c = d1 /. a1 in
      let qQ = ((a *. a) -. (3.0 *. b)) /. 9.0
      and rR =
        ((2.0 *. a *. a *. a) -. (9.0 *. a *. b) +. (27.0 *. c)) /. 54.0
      in
      let delta = (qQ *. qQ *. qQ) -. (rR *. rR) in
      if delta >= 0.0 then
        let theta = acos (rR /. sqrt (qQ *. qQ *. qQ)) in
        let tk k =
          (-.a /. 3.0)
          -. (2.0 *. sqrt qQ *. cos ((theta /. 3.0) +. (2.0 *. pi *. k /. 3.0)))
        in
        let t0 = tk 0.0 and t1 = tk (-1.0) and t2 = tk 1.0 in
        [ t0; t1; t2 ]
      else
        let sr = if rR < 0.0 then -1.0 else if rR = 0.0 then 0.0 else 1.0 in
        let aA = sr *. sqrt (abs_float rR +. sqrt (-.delta)) in
        let bB = if aA = 0.0 then 0.0 else qQ /. aA in
        let t = aA +. bB -. (aA /. 3.0) in
        [ t ]

  let is_over tos mouse_pos ~thick bez =
    let (xa, ya), (xb, yb), (xc, yc), d = poly_of tos bez in
    let xd, yd = d -.. mouse_pos in
    let epsy t =
      let y = (ya *. t *. t *. t) +. (yb *. t *. t) +. (yc *. t) +. yd in
      t >= 0.0 && t <= 1.0 && abs_float y <= 1.5 *. thick
    in
    let epsx t =
      let x = (xa *. t *. t *. t) +. (xb *. t *. t) +. (xc *. t) +. xd in
      t >= 0.0 && t <= 1.0 && abs_float x <= 1.5 *. thick
    in
    let b =
      List.exists epsy (solve_cubic xa xb xc xd)
      || List.exists epsx (solve_cubic ya yb yc yd)
    in
    b

  let point_at t ?(offset = 0.0) bez =
    let a, b, c, d = poly_of (fun x -> x) bez in
    let no = normal @@ (mult (3.0 *. t *. t) a +.. mult (2.0 *. t) b +.. c) in

    mult (t *. t *. t) a
    +.. mult (t *. t) b
    +.. mult t c +.. d +.. mult offset no
end

module Arrow = struct
  type t = point * point

  let draw to_screen ctx ~thick (center, origin) =
    let rho = angle (center -.. origin) in
    let xo, yo = to_screen center in
    let xp1, yp1 = to_screen @@ (center +.. rot rho (-6.0, 3.0))
    and xp2, yp2 = to_screen @@ (center +.. rot rho (-6.0, -3.0)) in
    ctx##.lineWidth := Js.float thick;
    ctx##beginPath;
    ctx ## (moveTo (Js.float xo) (Js.float yo));
    ctx ## (lineTo (Js.float xp1) (Js.float yp1));
    ctx ## (lineTo (Js.float xp2) (Js.float yp2));
    ctx ## (lineTo (Js.float xo) (Js.float yo));
    ctx##fill;
    ctx##stroke
end

module SimpleArrow = struct
  type t = point * point

  let draw to_screen ctx ~thick (center, origin) =
    let rho = angle (center -.. origin) in
    let xo, yo = to_js_float @@ to_screen center in
    let xp1, yp1 = to_js_float @@ to_screen @@ (center +.. rot rho (-4.0, 4.0))
    and xp2, yp2 = to_js_float @@ to_screen @@ (center +.. rot rho (-4.0, -4.0)) in
    ctx##.lineWidth := Js.float thick;
    ctx##beginPath;
    ctx ## (moveTo xp1 yp1);
    ctx ## (lineTo xo yo);
    ctx ## (lineTo xp2 yp2);
    ctx##stroke
end

module RoundArrow = struct
  type t = point * point

  let draw to_screen ctx ~thick (center, origin) =
    let rho = angle (center -.. origin) in
    let xp1, yp1 = to_js_float @@ to_screen @@ (center +.. rot rho (-4.0, 0.0)) in
    ctx##.lineWidth := Js.float thick;
    ctx##beginPath;
    ctx ## (arc xp1 yp1 (Js.float 4.0) (Js.float 0.0) (Js.float 6.28318530717958) (Js.bool true));
    ctx##fill;
    ctx##stroke
end

type shape =
  [ `Empty
  | `Circle of Circle.t
  | `Rectangle of Rectangle.t
  | `RoundedRectangle of RoundedRectangle.t
  | `Text of Text.t
  | `TokenSet of TokenSet.t
  | `Colors of Color.t * Color.t
  | `Line of Line.t
  | `Bezier2 of Bezier2.t
  | `Bezier3 of Bezier3.t
  | `Arrow of Arrow.t
  | `SimpleArrow of SimpleArrow.t
  | `RoundArrow of RoundArrow.t ]

type path =
  [ `Empty
  | `Colors of Color.t * Color.t
  | `Line of Line.t
  | `Bezier2 of Bezier2.t
  | `Bezier3 of Bezier3.t
  | `Arrow of Arrow.t
  | `SimpleArrow of SimpleArrow.t
  | `RoundArrow of RoundArrow.t ]

let center_shape = function
  | `Empty -> (0.0, 0.0)
  | `Circle (pos, _) -> pos
  | `Rectangle (pos, _, _, _) -> pos
  | `RoundedRectangle (pos, _, _, _) -> pos
  | `Text (pos, _) -> pos
  | `TokenSet (pos, _, _) -> pos
  | `Line (pos1, pos2) -> mult 0.5 (pos1 +.. pos2)
  | `Bezier2 (pos1, pos2, pos3) -> mult 0.33 (pos1 +.. pos2 +.. pos3)
  | _ -> (0.0, 0.0)

let rec tangible = function
  | [] -> `Empty
  | `Circle c :: _ -> `Circle c
  | `Rectangle r :: _ -> `Rectangle r
  | `RoundedRectangle r :: _ -> `RoundedRectangle r
  | `Text t :: _ -> `Text t
  | _ :: q -> tangible q

let center_shapes sl = center_shape @@ tangible sl

let projection_shape origin = function
  | `Circle c -> Circle.projection c origin
  | `Rectangle r -> Rectangle.projection r origin
  | `RoundedRectangle rr -> RoundedRectangle.projection rr origin
  | x -> center_shape x

let draw_shape tos ctx ?(thick = 3.0) = function
  | `Empty -> ()
  | `Circle c -> Circle.draw tos ctx ~thick c
  | `Rectangle r -> Rectangle.draw tos ctx ~thick r
  | `RoundedRectangle rr -> RoundedRectangle.draw tos ctx ~thick rr
  | `Text t -> Text.draw tos ctx ~thick t
  | `TokenSet ts -> TokenSet.draw tos ctx ~thick ts
  | `Colors (c1, c2) ->
      Canvas.setStrokeColor ctx c1;
      Canvas.setFillColor ctx c2
  | `Line l -> Line.draw tos ctx ~thick l
  | `Bezier2 l -> Bezier2.draw tos ctx ~thick l
  | `Bezier3 l -> Bezier3.draw tos ctx ~thick l
  | `Arrow a -> Arrow.draw tos ctx ~thick a
  | `SimpleArrow a -> SimpleArrow.draw tos ctx ~thick a
  | `RoundArrow a -> RoundArrow.draw tos ctx ~thick a

let print_shape f = function
  | `Empty -> ()
  | `Line (x, y) -> Printf.fprintf f "%a--%a" print_point x print_point y
  | `Bezier2 (x, y, z) ->
      Printf.fprintf f "%a-(%a)-%a" print_point x print_point y print_point z
  | `Arrow (x, y) -> Printf.fprintf f "%a->%a" print_point x print_point y

let draw_shapes tos ctx ?(thick = 3.0) ls =
  List.iter (draw_shape tos ctx ~thick) ls

let is_over_shape tos mouse_pos = function
  | `Empty -> false
  | `Circle c -> Circle.is_over tos mouse_pos c
  | `Rectangle r -> Rectangle.is_over tos mouse_pos r
  | `RoundedRectangle rr -> RoundedRectangle.is_over tos mouse_pos rr
  | `Line l -> Line.is_over ~thick:3.0 tos mouse_pos l
  | `Bezier2 b -> Bezier2.is_over ~thick:3.0 tos mouse_pos b
  | `Bezier3 b -> Bezier3.is_over ~thick:3.0 tos mouse_pos b
  | _ -> false

let is_over_shapes tos mouse_pos ls =
  List.fold_left (fun b sh -> b || is_over_shape tos mouse_pos sh) false ls

type path_elem =
  [ `Point of point | `ControlPoint of point | `Text of float * float * string ]

let point_of_path_elem = function `Point p -> p | `ControlPoint p -> p

let project_path first_shape point_list last_shape =
  let first, last =
    match point_list with
    | [] -> (center_shape last_shape, center_shape first_shape)
    | [ t ] -> (t, t)
    | t :: q -> (List.hd (List.rev q), t)
  in
  let pos2 = projection_shape first last_shape in
  let pos1 = projection_shape last first_shape in
  if point_list = [] then (pos1, [ first ], pos2) else (pos1, point_list, pos2)

let shapes_of_path shape1 ?arrow1 (point_list : path_elem list) ?arrow2 shape2 =
  let first, last =
    match
      List.fold_right
        (fun elem l ->
          match elem with
          | `Point p -> p :: l
          | `ControlPoint p -> p :: l
          | `Text _ -> l)
        point_list []
    with
    | [] -> (center_shape shape1, center_shape shape2)
    | [ t ] -> (t, t)
    | t :: q -> (List.hd (List.rev q), t)
  in
  let pos2 = projection_shape first shape2 in
  let pos1 = projection_shape last shape1 in
  let _, sl, _ =
    List.fold_left
      (fun (pp, l, ltext) x ->
        match (pp, x) with
        | _, `Text (t, o, m) -> (pp, l, (t, o, m) :: ltext)
        | `Text _ :: q, _ -> (q, l, ltext)
        | `Point pi :: _, `Point pip ->
            let line = (pi, pip) in
            let l3 =
              List.fold_left
                (fun l2 (t, o, m) ->
                  let post = Line.point_at t ~offset:o line in
                  `Text (post, m) :: l2)
                l ltext
            in
            ([ x ], `Line (pi, pip) :: l3, [])
        | `ControlPoint p2 :: `Point p1 :: _, `Point pip ->
            let bez = (p1, p2, pip) in
            let l3 =
              List.fold_left
                (fun l2 (t, o, m) ->
                  let post = Bezier2.point_at t ~offset:o bez in
                  `Text (post, m) :: l2)
                l ltext
            in
            ([ x ], `Bezier2 bez :: l3, [])
        | `ControlPoint p3 :: `ControlPoint p2 :: `Point p1 :: _, `Point pip ->
            let bez = (p1, p2, p3, pip) in
            let l3 =
              List.fold_left
                (fun l2 (t, o, m) ->
                  let post = Bezier3.point_at t ~offset:o bez in
                  `Text (post, m) :: l2)
                l ltext
            in
            ([ x ], `Bezier3 bez :: l3, [])
        | ( `ControlPoint p3 :: `ControlPoint p2 :: `Point p1 :: _,
            `ControlPoint p4 ) ->
            let m = mult 0.5 (p4 +.. p3) in
            let bez = (p1, p2, p3, m) in
            let l3 =
              List.fold_left
                (fun l2 (t, o, m) ->
                  let post = Bezier3.point_at t ~offset:o bez in
                  `Text (post, m) :: l2)
                l ltext
            in
            ([ `ControlPoint p4; `Point m ], `Bezier3 bez :: l3, [])
        | hist, `ControlPoint _ -> (x :: hist, l, ltext)
        | _ -> failwith "Unsupported path shape")
      ( [ `Point pos1 ],
        ((match arrow1 with None -> [] | Some f -> [ f pos1 last ])
        @ match arrow2 with None -> [] | Some f -> [ f pos2 first ]),
        [] )
      (point_list @ [ `Point pos2 ])
  in
  (sl : shape list)
