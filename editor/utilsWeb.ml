open Js_of_ocaml

type t = Dom.node Js.t

let txt value =
  let text = Dom_html.document##createTextNode (Js.string value) in
  (text :> t)

let append_node parent node =
  let (_ : Dom.node Js.t) = parent##appendChild node in
  ()

let br () =
  let br = Dom_html.(createBr document) in
  (br :> t)

let ul ?id items =
  let ul = Dom_html.(createUl document) in
  (match id with None -> () | Some ids -> ul##.id := Js.string ids);
  List.iter (append_node ul) items;
  (ul :> t)

let tr ?id items =
  let tr = Dom_html.(createTr document) in
  (match id with None -> () | Some ids -> tr##.id := Js.string ids);
  List.iter (append_node tr) items;
  (tr :> t)

let td ?colspan ?id ?(title = "") items =
  let td = Dom_html.(createTd document) in
  (match id with None -> () | Some ids -> td##.id := Js.string ids);
  (match colspan with Some x -> td##.colSpan := x | None -> ());
  List.iter (append_node td) items;
  td##.title := Js.string title;
  (td :> t)

let table ?id items =
  let table = Dom_html.(createTable document) in
  (match id with None -> () | Some ids -> table##.id := Js.string ids);
  List.iter (append_node table) items;
  (table :> t)

let div_raw ?(class_ = "") ?(title = "") ?id items =
  let div = Dom_html.(createDiv document) in
  List.iter (append_node div) items;
  (match id with None -> () | Some ids -> div##.id := Js.string ids);
  div##.title := Js.string title;
  div##.className := Js.string class_;
  div

let div ?(class_ = "") ?(title = "") ?id items =
  let div = Dom_html.(createDiv document) in
  List.iter (append_node div) items;
  (match id with None -> () | Some ids -> div##.id := Js.string ids);
  div##.title := Js.string title;
  div##.className := Js.string class_;
  (div :> t)

let p ?(class_ = "") items =
  let p = Dom_html.(createP document) in
  List.iter (append_node p) items;
  p##.className := Js.string class_;
  (p :> t)

let button ?(class_ = "") ?(on_click = fun () -> ()) items =
  let button = Dom_html.(createButton document) in
  let append_node node =
    let (_ : Dom.node Js.t) = button##appendChild node in
    ()
  in
  List.iter append_node items;
  button##.className := Js.string class_;
  let on_click _ =
    on_click ();
    Js._true
  in
  button##.onclick := Dom.handler on_click;
  (button :> t)

let option ?(def = false) value =
  let opt = Dom_html.(createOption document) in
  (*opt##.value := Js.string value;*)
  append_node opt (txt value);
  if def then opt##.defaultSelected := Js._true;
  (opt :> t)

let choice_input ?(class_ = "") ?(init_value = None) ?(on_change = fun _ -> ())
    sl =
  let sel = Dom_html.(createSelect document) in
  List.iter
    (fun s ->
      append_node sel
        (option
           ~def:(match init_value with None -> false | Some x -> x = s)
           s))
    (">" :: sl);
  let on_input _ =
    on_change (Js.to_string sel##.value);
    Js._true
  in
  sel##.oninput := Dom.handler on_input;
  sel##.className := Js.string class_;
  (sel :> t)

let check_input ?(class_ = "") ?(on_change = fun _ -> true) value =
  let input = Dom_html.(createInput ~_type:(Js.string "checkbox") document) in
  input##.checked := Js.bool value;
  let on_input _ =
    let b = string_of_bool (Js.to_bool input##.checked) in
    let _ = on_change b in
    Js._true
  in
  input##.oninput := Dom.handler on_input;
  input##.className := Js.string class_;
  (input :> t)

let text_input ?(class_ = "") ?(id = "") ?(on_change = fun _ -> true)
    ?(_type = "text") value =
  (*let input2 = input ~a:[a_input_type `Text] () in
    let input = Eliom_content.Html.To_dom.of_input input2 in*)
  let input = Dom_html.(createInput ~_type:(Js.string _type) document) in
  input##.value := Js.string value;
  input##.id := Js.string id;
  let on_input _ =
    let res = on_change (Js.to_string input##.value) in
    if res then input##.style##.color := Js.string "green"
    else input##.style##.color := Js.string "red";
    Js._true
  in
  input##.oninput := Dom.handler on_input;
  input##.className := Js.string class_;
  (input :> t)

let update_link n f =
  let link = Dom_html.(createA document) in
  append_node link (txt n);
  let currref = Js.to_string Dom_html.window##.location##.pathname in
  link##.onmouseover :=
    Dom.handler (fun _ ->
        let string_value = f currref in
        link##.href := Js.string string_value;
        Js._true);
  (link :> t)

let color_input ?(class_ = "") ?(on_change = fun _ -> true) value =
  let input = Dom_html.(createInput ~_type:(Js.string "color") document) in
  (*let input2 = input ~a:[a_input_type `Color] () in
    let input = Eliom_content.Html.To_dom.of_input input2 in*)
  input##.value :=
    Js.string (DrawingGeom.Color.to_string (DrawingGeom.Color.parse value));
  let on_input _ =
    let res = on_change (Js.to_string input##.value) in
    if res then input##.style##.color := Js.string "green"
    else input##.style##.color := Js.string "red";
    Js._true
  in
  input##.oninput := Dom.handler on_input;
  input##.className := Js.string class_;
  (input :> t)

let change_file (cb : string -> unit) _ =
  let id2 = Js.string "filein" in
  let f s = cb (Js.to_string s) in
  Js.Unsafe.fun_call
    (Js.Unsafe.js_expr "upload")
    [| Js.Unsafe.inject f; Js.Unsafe.inject id2 |]

let rec split_attr s i =
  match String.index_from_opt s i '=' with
  | Some j -> (
      let opt = String.sub s i (j - i) in
      match String.index_from_opt s (j + 1) '&' with
      | Some k -> (opt, String.sub s (j + 1) (k - j - 1)) :: split_attr s (k + 1)
      | None -> [ (opt, String.sub s (j + 1) (String.length s - j - 1)) ])
  | None -> []

let getURL s =
  let query = Js.to_string Dom_html.window##.location##.search in
  let m = split_attr query 1 in
  List.assoc_opt s m

let text_area' ?(class_ = "") ?(on_change = fun _ -> ()) ?(is_read_only = false)
    value =
  let input = Dom_html.(createTextarea document) in
  input##.value := Js.string value;
  if is_read_only then
    input##setAttribute (Js.string "readonly") (Js.string "true");
  let on_input _ =
    on_change (Js.to_string input##.value);
    Js._true
  in
  input##.oninput := Dom.handler on_input;
  input##.className := Js.string class_;
  let set_value value = input##.value := Js.string value in
  ((input :> t), set_value)

let text_area ?class_ ?on_change ?is_read_only:_ value =
  let text_area, _ = text_area' ?class_ ?on_change value in
  text_area

let run html =
  let on_load _ =
    let html = html () in
    let body =
      let find_tag name =
        let elements =
          Dom_html.window##.document##getElementsByTagName (Js.string name)
        in
        let element =
          Js.Opt.get
            (elements##item 0)
            (fun () -> failwith ("find_tag(" ^ name ^ ")"))
        in
        element
      in
      find_tag "body"
    in
    let (_ : t) = body##appendChild html in
    Js._false
  in
  Dom_html.window##.onload := Dom.handler on_load

let run' html =
  let on_load _ =
    let _ = html () in
    Js._false
  in
  Dom_html.window##.onload := Dom.handler on_load
