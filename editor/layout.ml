open DrawingGeom
open Point

let diff_gaussian k p0 p =
  let vect = p0 -.. p in
  mult (-2.0 *. gaussian k vect /. k) vect

let diff_cone p0 p =
  let vect = p -.. p0 in
  if dist vect > 30.0 then (0.0, 0.0) else mult 2.0 vect

let gradient center (node_weight, node_dist, arc_weight, gravity_weight) k nodes
    arcs =
  let diff_node (node, pos) nodes arcs =
    let acc =
      ref
        (mult (-2.0 *. gravity_weight)
           ((fun (x, y) -> (x /. 2.0, y)) (pos -.. center)))
    in
    Array.iter
      (fun (n, pos2) ->
        if n <> node then
          acc := !acc +.. mult node_weight (diff_gaussian node_dist pos2 pos))
      nodes;
    List.iter
      (fun (_, i, j) ->
        let n1, pos1 = nodes.(i) and n2, pos2 = nodes.(j) in
        if n1 = node then acc := !acc +.. mult arc_weight (pos2 -.. pos);
        if n2 = node then acc := !acc +.. mult arc_weight (pos1 -.. pos))
      arcs;
    !acc
  in

  let nodes2 =
    Array.map
      (fun (node, pos) ->
        (node, pos +.. mult k (diff_node (node, pos) nodes arcs)))
      nodes
  in
  nodes2

(*#load "graphics.cma";;
  #load "unix.cma";;

  let to_screen pos =
    let x,y =  (250.0,250.0) +.. (mult 1.0 pos) in
    (int_of_float x),(int_of_float y)

  let draw_point c pos =
    Graphics.set_color c;
    let x,y = to_screen pos in
    Graphics.draw_circle x y 2

  let draw_graph c nodes arc =
    Array.iter (fun (_,pos) ->   draw_point c pos) nodes;
    List.iter (fun (_,i,j) ->
        let (_,pos1) = nodes.(i)
        and (_,pos2) = nodes.(j) in
        let x1,y1 = to_screen pos1
        and x2,y2 = to_screen pos2 in
        Graphics.moveto x1 y1;
        Graphics.lineto x2 y2) arc;;*)

let foa x =
  let i = Hashtbl.hash x in
  let x = ref (float i) and y = ref (float i) in
  while abs_float !x > 20.1 do
    x := 20.0 -. (!x /. 2.0)
  done;
  while abs_float !y > 20.1 do
    y := 20.0 -. (!y /. 3.0)
  done;
  (!x, !y)

let center_of_nodes nodes =
  let acc =
    Array.fold_left (fun acc (_, pos) -> acc +.. pos) (0.0, 0.0) nodes
  in
  mult (1.0 /. float (Array.length nodes)) acc

let node_weight = 25.0
let arc_weight = 0.0075
let gravity_weight = 0.001
let node_dist = 40.0

let layout_graph nodes arcs =
  let center = center_of_nodes nodes in
  let nodes2 = Array.map (fun (n, pos) -> (n, pos +.. foa n)) nodes in
  let nodesref = ref nodes2 in
  for _ = 0 to 100 do
    (*Array.iter (fun (_,pos) ->
        draw_point (Graphics.rgb 200 200 (10*i)) pos) (!nodesref);*)
    nodesref :=
      gradient center
        (node_weight, node_dist, arc_weight, gravity_weight)
        3.0 !nodesref arcs
    (*ignore @@ Unix.select [] [] [] 0.1*)
  done;
  let center2 = center_of_nodes !nodesref in
  let vect = center -.. center2 in
  (*Array.iter (fun (_,pos) ->   draw_point (Graphics.rgb 255 0 0) pos) !nodesref;*)
  Array.map (fun (n, pos) -> (n, pos +.. vect)) !nodesref

(*
draw_point (Graphics.rgb 0 0 255) (0.0,0.0);;
let arc = [ 0,0,1; 1,1,2; 2,2,0; 3,3,1; 4,3,0; 5,4,0; 6,5,4; 7,5,2 ];;

let nodes = let _ = Graphics.open_graph "" in
            layout_graph [| 0,(0.0,0.0);
                            1,(0.0,0.0);
                            2,(0.0,0.0);
                            3,(0.0,0.0);
                            4,(0.0,0.0);
                            5,(0.0,0.0);
              |] arc in
    draw_graph  (Graphics.rgb 0 255 0) nodes arc; nodes
    


let nodes2 = Array.map (fun (n,pos) -> (n, pos +.. foa n))  [| 0,(0.0,0.0);
                            1,(0.0,0.0);
                            2,(0.0,0.0);
                            3,(0.0,0.0);
                            4,(0.0,0.0);
              |];;
 *)
