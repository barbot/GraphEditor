%{

    %}

%token <int> INT
%token <float> FLOAT
%token <string> STRING
%token <string> IDENTIFIER
%token <string> TEXCONTENT
%token LPAR RPAR
%token BACKSLASH
%token SEMICOLON COLON PRIME COMMA QMARK EQ
%token LSQBRAK RSQBRAK
%token EOF
%token BEGIN END AND
%token TIKZPICTURE
%token NODE DRAW AT PATH CONTROLS
%token PATHDELIM CONTROLDELIM

%start main
%type <(((float*float)*(string list)*string*string) list)*((string list*string*string*(GraphEditor.DrawingGeom.path_elem) list ) list)> main
%%

main:
  BACKSLASH BEGIN TIKZPICTURE 
    body
     TIKZPICTURE
    { $4 } ;
      
body:
 BACKSLASH NODE node body {
  let ((x,y),_,name,content) = $3 in
  Printf.printf "\\node[] (%f,%f) %s %s;\n" x y name content;
  let n,a = $4 in
  $3 :: n , a
   }
    | BACKSLASH DRAW draw body {
let (_,start,fin,_) = $3 in
Printf.printf "\\draw %s -> %s;\n" start fin;
let n,a = $4 in n,$3::a }
    | BACKSLASH PATH draw body {
let (al,start,fin,pl) = $3 in
  Printf.printf "\\path %s -> %s;\n" start fin;
  let n,a = $4 in n,("draw"::al,start,fin,pl)::a }
     | BACKSLASH END { [],[] };
     
node:
    AT LPAR FLOAT COMMA FLOAT RPAR LSQBRAK attribute_list RSQBRAK LPAR IDENTIFIER RPAR TEXCONTENT SEMICOLON { ($3,$5),$8,$11,$13 };

draw:
  LSQBRAK attribute_list RSQBRAK LPAR IDENTIFIER  RPAR pathlist SEMICOLON {
      let pl,fin = $7 in ($2,$5,fin,pl) }

pathlist:
PATHDELIM LPAR FLOAT COMMA FLOAT RPAR pathlist
      { let l,f = $7 in (`Point ($3,$5)::l,f) }
  | CONTROLDELIM CONTROLS LPAR FLOAT COMMA FLOAT RPAR AND LPAR FLOAT COMMA FLOAT RPAR CONTROLDELIM pathlist2
      { let l,f = $15 in
      `ControlPoint ($4,$6)::`ControlPoint ($10,$12)::l,f  }
  | PATHDELIM LPAR IDENTIFIER RPAR { [],$3 }
  | NODE TEXCONTENT pathlist {
       let l,f = $3 in (`Text (0.5,8.0,$2)::l,f) }

pathlist2:
LPAR FLOAT COMMA FLOAT RPAR pathlist { let l,f = $6 in (`Point ($2,$4)::l,f) }
 | NODE TEXCONTENT pathlist2 {
       let l,f = $3 in (`Text (0.5,8.0,$2)::l,f) }
 | LPAR IDENTIFIER RPAR { [],$2 }
     
attribute_list:
|  attribute { [$1] }
|  attribute COMMA attribute_list { $1::$3 }
| {[]}

attribute:
| DRAW { "draw" }
| IDENTIFIER { $1 }
| IDENTIFIER IDENTIFIER { $1^" "^$2 }
| IDENTIFIER EQ TEXCONTENT { $1^"="^$3}
