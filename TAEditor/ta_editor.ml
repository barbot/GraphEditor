open Js_of_ocaml
open GraphEditor
open UtilsWeb
module TAGraphEd = GraphDrawing.Make (SimpleGraph.S (TAGraph))

let _ =
  let create_form = div [] in
  let canvas =
    let ca = Dom_html.(createCanvas document) in
    ca##.width := 2048;
    ca##.height := 1024;
    ca
  in
  let slider =
    let sl = Dom_html.(createInput ~_type:(Js.string "range") document) in
    sl##.value := Js.string "40";
    sl##.id := Js.string "zoomslider";
    sl
  in
  let attribute_list_div =
    let div = Dom_html.(createDiv document) in
    append_node div (ul ~id:"attributelist" []);
    div
  in

  let attribute_div =
    div ~class_:"attributelistdiv" [ (attribute_list_div :> Dom.node Js.t) ]
  in

  let graph_editor =
    div ~class_:"row jumbotron"
      [
        div ~class_:"col-8"
          [
            create_form;
            div ~class_:"container"
              [
                (let dr =
                   div ~class_:"graph_editor_canvas"
                     [ (canvas :> Dom.node Js.t) ]
                 in
                 (*dr##.oncontextmenu := (fun _ -> ());*)
                 (dr :> Dom.node Js.t));
                div ~class_:"zoom_slider"
                  [ txt "zoom: "; (slider :> Dom.node Js.t) ];
              ];
          ];
        div ~class_:"col-4" [ attribute_div ];
      ]
  in
  run @@ fun () ->
  let ed =
    TAGraphEd.init ~saveload:create_form ~slider canvas attribute_list_div
  in
  TAGraphEd.init_client ed;
  graph_editor
