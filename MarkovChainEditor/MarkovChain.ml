open GraphEditor

type def = unit
type state = string * string list
type arc = float
type attribute_id = int

type attribute =
  [ `Choice of string list
  | `Check of bool
  | `ControlPoint of DrawingGeom.point
  | `String of string
  | `Color of string ]

let init_def () = ()
let init_arc _ _ = Some 1.0
let state_id = ref 0

let init_state () =
  incr state_id;
  ("s" ^ string_of_int !state_id, [])

let draw_state (s, _) p =
  [ `RoundedRectangle (p, 10.0, 0.75, 5.0); `Text (p, s) ]

let draw_arc arc (source_sh, target_sh) =
  let open DrawingGeom in
  let open Point in
  let pos1 = center_shape source_sh in
  let pos2 = center_shape target_sh in
  let text = string_of_float arc in

  if source_sh <> target_sh then
    shapes_of_path source_sh
      [ `Text (0.5, 8.0, text) ]
      ~arrow2:(fun x y -> `Arrow (x, y))
      target_sh
  else
    let p1 = pos1 +.. (40.0, -25.0) and p2 = pos2 +.. (40.0, 25.0) in
    shapes_of_path source_sh
      [ `ControlPoint p1; `Text (0.5, 8.0, text); `ControlPoint p2 ]
      ~arrow2:(fun x y -> `Arrow (x, y))
      target_sh

let get_state_attr (s, at) =
  ( "Node",
    (0, "content", None, `String s)
    :: List.mapi
         (fun i sv ->
           (i + 1, "attribute " ^ string_of_int (i + 1), None, `String sv))
         at )

let update_state_attr (s, at) attr_id = function
  | None ->
      if attr_id = 0 then Some ("", at)
      else
        let _, listat =
          List.fold_left
            (fun (i, l) at ->
              if attr_id = i + 1 then (i + 1, l) else (i + 1, at :: l))
            (0, []) at
        in
        Some (s, listat)
  | Some (`String newv) ->
      if attr_id = 0 then Some (newv, at)
      else
        let _, listat =
          List.fold_left
            (fun (i, l) at ->
              if attr_id = i + 1 then (i + 1, newv :: l) else (i + 1, at :: l))
            (0, []) at
        in
        Some (s, listat)
  | _ -> None

let get_new_state_attr (_, _) _ = []

let get_arc_attr prob =
  ("Arc", [ (0, "Probability", None, `String (string_of_float prob)) ])

let update_arc_attr _ _ = function
  | None -> (
      try
        let p = 1.0 in
        if p >= 0.0 && p <= 1.0 then Some p else None
      with _ -> None)
  | Some (`String v) -> (
      try
        let p = float_of_string v in
        if p >= 0.0 && p <= 1.0 then Some p else None
      with _ -> None)
  | _ -> None

let get_new_arc_attr _ _ = []
let get_new_def_attr _ = []
let get_def_attr _ = ("", [])
let update_def_attr _ _ _ = None

let string_of_attribute = function
  | `Prob arc -> string_of_float arc
  | `StringExpr s -> s
  | `Choice (t :: _) -> t
  | `Choice [] -> ""
  | `ControlPoint _ -> ""

let print =
  [
    ( "dot",
      (fun f _ stateit arcit ->
        Format.fprintf f "digraph {\n";
        stateit (fun _ (s, _) (x, y) ->
            Format.fprintf f "%s [pos=\"%f,%f\"];\n" s x y);
        arcit (fun _ _ ((_, (source, _), _), (_, (target, _), _)) ->
            Format.fprintf f "%s -> %s;\n" source target);
        Format.fprintf f "}"),
      "markovChain.dot" );
  ]

let parse_file _ _ _ = ()
