open GraphEditor
open PetriNet
open Type
open DrawingGeom

type graph = GrMLParser.gen_spt

type node =
  [ `Empty
  | `Place of Net.placekey Data.key
  | `Transition of Net.transitionkey Data.key ]

type arc =
  [ `Empty
  | `InArc of Net.inArcKey Data.key
  | `InhibArc of Net.inhibArcKey Data.key
  | `OutArc of Net.outArcKey Data.key ]

type attribute_id = int

type attribute =
  [ `String of string
  | `Choice of string list
  | `Check of bool
  | `ControlPoint of DrawingGeom.point
  | `Color of string ]

let pls p = `Circle (p, 10.0)
let trs p = `Rectangle (p, 3.0, 10.0, 0.0)
let trhs p = `Rectangle (p, 0.3333, 3.3333, 0.0)
let string_of_attribute v = Format.asprintf "%a" GrMLParser.printH_attr_val v

let rec parse_attribute oldAttr newAttr =
  let lexbuf = Lexing.from_string newAttr in
  (*Printf.printf "parsing:%s\n" newAttr;*)
  match oldAttr with
  | `StringExpr _ -> `StringExpr newAttr
  | `IntExpr _ -> `IntExpr (ParserPrism.intexpr LexerPrism.token lexbuf)
  | `FloatExpr _ ->
      let e =
        try ParserPrism.floatexpr LexerPrism.token lexbuf
        with _ ->
          let lexbuf2 = Lexing.from_string newAttr in
          CastInt (ParserPrism.intexpr LexerPrism.token lexbuf2)
      in
      `FloatExpr e
  | `BoolExpr _ ->
      `BoolExpr (ParserPrism.stateCondition LexerPrism.token lexbuf)
  | `Declaration (_, x) ->
      let i = String.index newAttr ':' in
      let vn = String.sub newAttr 0 i in
      let vv = String.sub newAttr (i + 1) (String.length newAttr - i - 1) in
      let nv = parse_attribute x vv in
      `Declaration (vn, nv)
  | `DistrExpr _ ->
      let ei =
        match ParserPrism.distrexpr LexerPrism.token lexbuf with
        | "EXPONENTIAL", [ f ] -> StochasticPetriNet.Exp f
        | "IMMEDIATE", [] -> StochasticPetriNet.Imm
        | "IMDT", [] -> StochasticPetriNet.Imm
        | "MASSACTION", [ f ] -> StochasticPetriNet.Exp f
        | "DETERMINISTIC", [ f ] -> StochasticPetriNet.Det f
        | "UNIFORM", [ f1; f2 ] -> StochasticPetriNet.Unif (f1, f2)
        | "ERLANG", [ i; f ] -> StochasticPetriNet.Erl (Floor i, f)
        | "GEOMETRIC", [ f1; f2 ] -> StochasticPetriNet.Geometric (f1, f2)
        | "LOGNORMAL", [ f1; f2 ] -> StochasticPetriNet.LogNormal (f1, f2)
        | x, _ -> failwith ("ill define distribution" ^ x)
      in
      `DistrExpr ei
  | `UnParsed _ -> `UnParsed newAttr

(* | `Choice l -> `Choice l
   | `ControlPoint x -> `ControlPoint x*)

let rec list_map_once f = function
  | [] -> []
  | t :: q -> (
      match f t with None -> t :: list_map_once f q | Some t2 -> t2 :: q)

let rec list_filter_once f = function
  | [] -> []
  | t :: q when f t -> t :: list_filter_once f q
  | _ :: q -> q

let apply_i j f l =
  List.rev @@ snd
  @@ List.fold_left
       (fun (i, l) e -> if i = j then (i + 1, f e :: l) else (i + 1, e :: l))
       (0, []) l

let replace_i j v l = apply_i j (fun _ -> v) l

let filter_i j l =
  List.rev @@ snd
  @@ List.fold_left
       (fun (i, l) e -> if i = j then (i + 1, l) else (i + 1, e :: l))
       (0, []) l

let update_node_attribute net node attr_id nattr =
  match nattr with
  | Some (`String attr_value) -> (
      let open GrMLParser in
      let filter_node = function
        | name, value -> (name, parse_attribute value attr_value)
      in
      try
        match node with
        | `Empty -> true
        | `Place pt ->
            let attl = snd (Data.acca net.Net.place pt) in
            attl.attr_list <- apply_i attr_id filter_node attl.attr_list;
            true
        | `Transition tr ->
            let attl = snd (Data.acca net.Net.transition tr) in
            attl.attr_list <- apply_i attr_id filter_node attl.attr_list;
            true
      with _ ->
        print_endline ("Fail to parse:'" ^ attr_value ^ "'");
        false)
  | None -> (
      match node with
      | `Empty -> true
      | `Place pt ->
          let attl = snd (Data.acca net.Net.place pt) in
          attl.attr_list <- filter_i attr_id attl.attr_list;
          true
      | `Transition tr ->
          let attl = snd (Data.acca net.Net.transition tr) in
          attl.attr_list <- filter_i attr_id attl.attr_list;
          true)
  | _ -> false

let update_arc_attribute net arc attr_id nattr =
  if arc = `Empty then
    let attr_name, _ = List.nth (net.Net.def |>>| []) attr_id in
    match nattr with
    | Some (`String attr_value) ->
        let pv =
          if attr_name = "intConst" then
            parse_attribute (`Declaration ("", `IntExpr (Int 0))) attr_value
          else
            parse_attribute
              (`Declaration ("", `FloatExpr (Float 0.0)))
              attr_value
        in
        net.Net.def <-
          Some (replace_i attr_id (attr_name, pv) (net.Net.def |>>| []));
        true
    | None ->
        net.Net.def <- Some (filter_i attr_id (net.Net.def |>>| []));
        true
    | _ -> false
  else if attr_id = 0 then
    let val_expr =
      match nattr with
      | Some (`String attr_value) ->
          let lexbuf = Lexing.from_string attr_value in
          ParserPrism.intexpr LexerPrism.token lexbuf
      | _ -> Int 1
    in
    match arc with
    | `InArc ia ->
        let (), (_, p, t) = Data.acca net.Net.inArc ia in
        Data.updatea ia (val_expr, p, t) net.Net.inArc;
        true
    | `OutArc oa ->
        let (), (_, p, t) = Data.acca net.Net.outArc oa in
        Data.updatea oa (val_expr, p, t) net.Net.outArc;
        true
    | `InhibArc iha ->
        let (), (_, p, t) = Data.acca net.Net.inhibArc iha in
        Data.updatea iha (val_expr, p, t) net.Net.inhibArc;
        true
    | `Empty -> false
  else
    match (arc, nattr) with
    | `InArc ia, Some (`Choice ("Inhibitor Arc" :: _)) ->
        let (), (v, p, t) = Data.acca net.Net.inArc ia in
        Data.remove net.Net.inArc ia;
        Data.add ((), (v, p, t)) net.Net.inhibArc;
        true
    | `InhibArc iha, Some (`Choice ("In Arc" :: _)) ->
        let (), (v, p, t) = Data.acca net.Net.inhibArc iha in
        Data.remove net.Net.inhibArc iha;
        Data.add ((), (v, p, t)) net.Net.inArc;
        true
    | _, _ -> false

let get_pos pl =
  let isHor =
    Some pl.GrMLParser.attr_list |>>> List.assoc "isHorizontal"
    |>>> (function `BoolExpr (Bool true) -> true | _ -> false)
    |>>| false
  in
  (pl.GrMLParser.position |>>| (0.0, 0.0), isHor)

let get_new_node_choice _ =
  [
    ((fun pos -> pls pos), 0);
    ((fun pos -> trs pos), 1);
    ((fun pos -> trhs pos), 2);
  ]

let shapes_of_node net n =
  let open GraphEditor.DrawingGeom.Point in
  match n with
  | `Empty -> [ `Empty ]
  | `Place pt -> (
      let _, pval = Data.acca net.Net.place pt in
      let pp, _ = get_pos pval in
      let mexpr = Some pval.GrMLParser.attr_list |>>> List.assoc "marking" in
      let sh =
        match mexpr with
        | None -> [ pls pp ]
        | Some (`IntExpr (Int 0)) -> [ pls pp ]
        | Some (`IntExpr (Int x)) when x > 0 && x <= 7 ->
            [
              pls pp;
              `Colors ((0, 0, 0), (0, 0, 0));
              `TokenSet (pp, 5.5, x);
              `Colors ((0, 0, 0), (255, 255, 255));
            ]
        | Some (`IntExpr v) ->
            let ms =
              Format.asprintf "%a" GrMLParser.printH_attr_val (`IntExpr v)
            in
            if String.length ms < 7 then
              [
                pls pp;
                `Colors ((0, 0, 0), (0, 0, 0));
                `Text (pp, ms);
                `Colors ((0, 0, 0), (255, 255, 255));
              ]
            else [ pls pp; `Text (pp, "..") ]
        | _ -> failwith "Ill structured Marking"
      in
      let name =
        Some pval.GrMLParser.attr_list |>>> List.assoc "name" |>> function
        | `StringExpr s -> Some s
        | _ -> None
      in
      match name with
      | None -> sh
      | Some n ->
          sh
          @ [
              `Colors ((0, 0, 0), (0, 0, 0));
              `Text (pp -.. (0.0, 14.0), n);
              `Colors ((0, 0, 0), (255, 255, 255));
            ])
  | `Transition tr -> (
      let _, tval = Data.acca net.Net.transition tr in
      let pp, isHor = get_pos tval in
      let shape = if isHor then trhs pp else trs pp in
      let name =
        Some tval.GrMLParser.attr_list |>>> List.assoc "name" |>> function
        | `StringExpr s -> Some s
        | _ -> None
      in
      match name with
      | None -> [ shape ]
      | Some n ->
          [
            shape;
            `Colors ((0, 0, 0), (0, 0, 0));
            `Text (pp -.. (0.0, 14.0), n);
            `Colors ((0, 0, 0), (255, 255, 255));
          ])

let nodes_of_arc net = function
  | `Empty -> (`Empty, `Empty)
  | `InArc ik ->
      let (), (_, p, t) = Data.acca net.Net.inArc ik in
      (`Place p, `Transition t)
  | `InhibArc ihk ->
      let (), (_, p, t) = Data.acca net.Net.inhibArc ihk in
      (`Place p, `Transition t)
  | `OutArc ok ->
      let (), (_, t, p) = Data.acca net.Net.outArc ok in
      (`Transition t, `Place p)

let valuation_of_arc net = function
  | `Empty -> ""
  | `InArc ik ->
      let (), (i, _, _) = Data.acca net.Net.inArc ik in
      Format.asprintf "%a" printH_expr i
  | `InhibArc ihk ->
      let (), (i, _, _) = Data.acca net.Net.inhibArc ihk in
      Format.asprintf "%a" printH_expr i
  | `OutArc ok ->
      let (), (i, _, _) = Data.acca net.Net.outArc ok in
      Format.asprintf "%a" printH_expr i

let iter_node net f =
  Data.iteri (fun plk _ -> f (`Place plk) 0) net.Net.place;
  Data.iteri (fun plk _ -> f (`Transition plk) 1) net.Net.transition

let shapes_of_arc net shape1 shape2 a =
  let open GraphEditor.DrawingGeom.Point in
  let p = mult 0.5 (center_shape shape1 +.. center_shape shape2) in
  let pos2 = projection_shape p shape2 in
  let pos1 = projection_shape pos2 shape1 in
  let middle = mult 0.5 (pos1 +.. pos2) in
  let rho = angle (pos1 -.. pos2) in
  let textvect = middle +.. rot rho (0.0, 8.0) in
  let l =
    let v = valuation_of_arc net a |> function "1" -> "" | x -> x in
    [
      `Colors ((0, 0, 0), (0, 0, 0));
      `Text (textvect, v);
      `Colors ((0, 0, 0), (255, 255, 255));
    ]
  in
  match a with
  | `Empty -> []
  | `InArc _ -> [ `Line (pos1, pos2); `Arrow (pos2, pos1) ] @ l
  | `OutArc _ -> [ `Line (pos1, pos2); `Arrow (pos2, pos1) ] @ l
  | `InhibArc _ -> [ `Line (pos1, pos2); `RoundArrow (pos2, pos1) ] @ l

let iter_arc net f =
  Data.iteri (fun ik _ -> f (`InArc ik)) net.Net.inArc;
  Data.iteri (fun ihk _ -> f (`InhibArc ihk)) net.Net.inhibArc;
  Data.iteri (fun ok _ -> f (`OutArc ok)) net.Net.outArc

let build_comment = function
  | "name" -> Some "Unique name"
  | "weight" ->
      Some
        "Weigth of transition, when transition occurs at same time and \
         priority the trasition is taken with probability corresponding to its \
         weight."
  | "priority" ->
      Some
        "Priority of transition, when transition occurs at same time the one \
         with highest priority is taken."
  | "distribution" ->
      Some
        "Time distribution, the available distribution are : EXPONENTIAL, \
         IMMEDIATE, MASSACTION, LOGNORMAL, DETERMINISTIC, UNIFORM"
  | _ -> None

let get_node_attribute net node =
  let f =
    List.mapi (fun i (n, x) ->
        (i, n, build_comment n, `String (string_of_attribute x)))
  in
  match node with
  | `Empty -> ("Declaration", net.Net.def |>>| [] |> f)
  | `Place plk ->
      let _, attr = Data.acca net.Net.place plk in
      (* ("^(string_of_int (Data.unsafe_rev plk))^") *)
      ("Place", f attr.GrMLParser.attr_list)
  | `Transition trk ->
      let _, attr = Data.acca net.Net.transition trk in
      ("Transition", f attr.GrMLParser.attr_list)

let get_arc_attribute net arc =
  let f x = `String (string_of_attribute x) in
  match arc with
  | `Empty ->
      ( "Declaration",
        net.Net.def |>>| [] |> List.mapi (fun i (n, x) -> (i, n, None, f x)) )
  | `InArc ik ->
      let _, (attr, _, _) = Data.acca net.Net.inArc ik in
      ( "In Arc",
        [
          ( 1,
            "type",
            Some "Normal arc or Inhibitor arc",
            `Choice [ "In Arc"; "Inhibitor Arc" ] );
          (0, "valuation", None, f (`IntExpr attr));
        ] )
  | `OutArc ik ->
      let _, (attr, _, _) = Data.acca net.Net.outArc ik in
      ( "Out Arc",
        [ (0, "valuation", Some "Valuation of the arc", f (`IntExpr attr)) ] )
  | `InhibArc ihk ->
      let _, (attr, _, _) = Data.acca net.Net.inhibArc ihk in
      ( "Inhibitor Arc",
        [
          ( 1,
            "type",
            Some "Normal arc or Inhibitor arc",
            `Choice [ "Inhibitor Arc"; "In Arc" ] );
          (0, "valuation", None, f (`IntExpr attr));
        ] )

let new_node net t pos =
  match t with
  | 0 ->
      `Place
        (Net.add_place net "new Place"
           {
             GrMLParser.position = Some pos;
             attr_list =
               [
                 ( "name",
                   `StringExpr ("P" ^ string_of_int @@ Data.size net.Net.place)
                 );
                 ("marking", `IntExpr (Int 0));
               ];
           })
  | x ->
      `Transition
        (Net.add_transition net "new Transition"
           {
             GrMLParser.position = Some pos;
             attr_list =
               [
                 ( "name",
                   `StringExpr
                     ("T" ^ string_of_int @@ Data.size net.Net.transition) );
                 ("isHorizontal", `BoolExpr (Bool (x = 2)));
                 ("distribution", `DistrExpr (Exp (Float 1.0)));
               ];
           })

let remove_node net = function
  | `Empty -> ()
  | `Place p ->
      Data.iteri
        (fun k ((), (_, p1, _)) -> if p = p1 then Data.remove net.Net.inArc k)
        net.Net.inArc;
      Data.iteri
        (fun k ((), (_, p1, _)) ->
          if p = p1 then Data.remove net.Net.inhibArc k)
        net.Net.inhibArc;
      Data.iteri
        (fun k ((), (_, _, p1)) -> if p = p1 then Data.remove net.Net.outArc k)
        net.Net.outArc;
      Data.remove net.Net.place p
  | `Transition t ->
      Data.iteri
        (fun k ((), (_, _, t1)) -> if t = t1 then Data.remove net.Net.inArc k)
        net.Net.inArc;
      Data.iteri
        (fun k ((), (_, _, t1)) ->
          if t = t1 then Data.remove net.Net.inhibArc k)
        net.Net.inhibArc;
      Data.iteri
        (fun k ((), (_, t1, _)) -> if t = t1 then Data.remove net.Net.outArc k)
        net.Net.outArc;
      Data.remove net.Net.transition t

let remove_arc net = function
  | `Empty -> ()
  | `InArc ia -> Data.remove net.Net.inArc ia
  | `OutArc ia -> Data.remove net.Net.outArc ia
  | `InhibArc ia -> Data.remove net.Net.inhibArc ia

let new_arc net t1 t2 =
  match (t1, t2) with
  | `Place p, `Transition t -> Some (`InArc (Net.add_inArc net p t (Int 1)))
  | `Transition t, `Place p -> Some (`OutArc (Net.add_outArc net t p (Int 1)))
  | _ -> None

let move_node net pos = function
  | `Empty -> ()
  | `Place p ->
      let _, po = Data.acca net.Net.place p in
      po.GrMLParser.position <- Some pos
  | `Transition t ->
      let _, tob = Data.acca net.Net.transition t in
      tob.GrMLParser.position <- Some pos

open GrMLParser

let download_file_name _ = "model.grml"

let print_graph =
  [
    ( "GrML",
      (fun f g ->
        let open StochPTPrinter in
        print_general_pt f
          (GrMLParser.print_gen_node "place")
          (GrMLParser.print_gen_node "transition")
          GrMLParser.print_gen_arc GrMLParser.print_gen_decl g),
      "model.grml" );
  ]

let read_graph s =
  let tree = Xml.parse_string s in
  GrMLParser.genPT_of_tree tree

let new_graph () : GrMLParser.gen_spt = PetriNet.Net.create ()

let get_new_node_attribute net = function
  | `Empty ->
      let cb s v () =
        let l = net.Net.def |>>| [] in
        net.Net.def <- Some ((s, v) :: l);
        0
      in
      [
        ("intConst", cb "intConst" (`Declaration ("ci", `IntExpr (Int 0))));
        ( "realConst",
          cb "realConst" (`Declaration ("cf", `FloatExpr (Float 0.0))) );
      ]
  | `Place p ->
      let attl = snd (Data.acca net.Net.place p) in
      let cb s v () =
        attl.attr_list <- (s, v) :: attl.attr_list;
        0
      in
      List.filter
        (fun (x, _) -> not (List.exists (fun (y, _) -> x = y) attl.attr_list))
        [
          ("marking", cb "marking" (`IntExpr (Int 1)));
          ( "name",
            cb "name" (`StringExpr ("P" ^ string_of_int (Data.unsafe_rev p))) );
        ]
  | `Transition t ->
      let attl = snd (Data.acca net.Net.transition t) in
      let cb s v () =
        attl.attr_list <- (s, v) :: attl.attr_list;
        0
      in
      List.filter
        (fun (x, _) -> not (List.exists (fun (y, _) -> x = y) attl.attr_list))
        [
          ( "name",
            cb "name" (`StringExpr ("T" ^ string_of_int (Data.unsafe_rev t))) );
          ("weight", cb "weight" (`FloatExpr (Float 1.0)));
          ("priority", cb "priority" (`FloatExpr (Float 1.0)));
          ("isHorizontal", cb "isHorizontal" (`BoolExpr (Bool false)));
          ("distribution", cb "distribution" (`DistrExpr (Exp (Float 1.0))));
        ]

let get_new_arc_attribute net = function
  | `Empty -> get_new_node_attribute net `Empty
  | _ -> []
