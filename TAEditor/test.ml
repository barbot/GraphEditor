let _ =
  let lexbuf = Lexer.from_file "figure.tikz" in
  lexbuf.lex_curr_p <- { lexbuf.lex_curr_p with pos_fname = name };
  try
    TikzParser.main TikzLexer.token lexbuf;
    print_endline "Finish first pass"
  with _ -> ()
