open Js_of_ocaml
open GraphEditor
open UtilsWeb
module TAGraph = SimpleGraph.S (TikzGraph)
module TAGraphEd = GraphDrawing.Make (TAGraph)

let _ =
  let create_form = div [] in
  let canvas =
    let ca = Dom_html.(createCanvas document) in
    ca##.width := 1024;
    ca##.height := 1024;
    ca
  in
  let slider =
    let sl = Dom_html.(createInput ~_type:(Js.string "range") document) in
    sl##.value := Js.string "40";
    sl##.id := Js.string "zoomslider";
    sl
  in
  let attribute_list_div =
    let div = Dom_html.(createDiv document) in
    append_node div (ul ~id:"attributelist" []);
    div
  in

  let attribute_div =
    div ~class_:"attributelistdiv" [ (attribute_list_div :> Dom.node Js.t) ]
  in

  let tikz_area, upta = text_area' ~class_:"col-8" "" in

  let graph_editor =
    div ~class_:"row jumbotron"
      [
        div ~class_:"col-8"
          [
            create_form;
            div ~class_:"container"
              [
                (let dr =
                   div_raw ~class_:"graph_editor_canvas"
                     [ (canvas :> Dom.node Js.t) ]
                 in
                 (*dr##.oncontextmenu := (fun _ -> ());*)
                 (dr :> Dom.node Js.t));
                div ~class_:"zoom_slider"
                  [ txt "zoom: "; (slider :> Dom.node Js.t) ];
              ];
          ];
        div ~class_:"col-4" [ attribute_div ];
        tikz_area;
      ]
  in
  run @@ fun () ->
  let ed =
    TAGraphEd.init ~saveload:create_form ~slider
      ~callback:(fun g ->
        let _, s, _ =
          List.find (fun (x, _, _) -> x = "tikz") TAGraph.print_graph
        in
        Format.fprintf Format.str_formatter "%a" s g;
        let content = Format.flush_str_formatter () in
        upta
          ("\\documentclass[tikz,border=10pt]{standalone}\\begin{document}"
         ^ content ^ "\\end{document}"))
      canvas attribute_list_div
  in
  TAGraphEd.init_client ed;
  graph_editor
