{
  open Lexing
  open TikzParser

  exception SyntaxError  of string

}

let digit = ['0'-'9']
let white = [' ' '\t']+
let newline = '\r' | '\n' | "\r\n"

rule token = parse
    white    {token lexbuf}
  | newline  {new_line lexbuf; token lexbuf}
  | ['/']['/'][^'\n']*    {token lexbuf}
  | digit+ as lxm  { INT(int_of_string lxm)}
  | ['-']?digit*['.']?digit+(['e' 'E']['-' '+']?digit+)? as lxm  {FLOAT(float_of_string lxm)}
  | '"'      { read_string (Buffer.create 17) lexbuf }
  | "'" {PRIME}
  | '(' {LPAR}
  | ')' {RPAR}
  | '{' { read_tex_content (Buffer.create 17) lexbuf}
  | '\\' {BACKSLASH}
  | '[' {LSQBRAK}
  | ']' {RSQBRAK}
  | ';' {SEMICOLON}
  | ',' {COMMA}
  | '=' {EQ}
  | ":" {COLON}
  | "--" {PATHDELIM}
  | ".." {CONTROLDELIM}
  | "controls" {CONTROLS}
  | "begin" {BEGIN}
  | "end" {END}
  | "and" {AND}
  | "{tikzpicture}" {TIKZPICTURE}
  | "node" {NODE}
  | "draw" {DRAW}
  | "path" {PATH}
  | "at" {AT}
  | ['a'-'z' 'A'-'Z' '>' '-' '<' ][ '>' '-' '<' '_' 'a'-'z' 'A'-'Z' '0'-'9']* as lxm { IDENTIFIER(lxm)}
 
  | _ { raise (SyntaxError ("Unexpected char: " ^ Lexing.lexeme lexbuf)) }
  | eof  {EOF}
and read_string buf = parse
  | '"'       { STRING (Buffer.contents buf) }
  | '\\' '/'  { Buffer.add_char buf '/'; read_string buf lexbuf }
  | '\\' '\\' { Buffer.add_char buf '\\'; read_string buf lexbuf }
  | '\\' 'b'  { Buffer.add_char buf '\b'; read_string buf lexbuf }
  | '\\' 'f'  { Buffer.add_char buf '\012'; read_string buf lexbuf }
  | '\\' 'n'  { Buffer.add_char buf '\n'; read_string buf lexbuf }
  | '\\' 'r'  { Buffer.add_char buf '\r'; read_string buf lexbuf }
  | '\\' 't'  { Buffer.add_char buf '\t'; read_string buf lexbuf }
  | [^ '"' '\\']+
    { Buffer.add_string buf (Lexing.lexeme lexbuf);
      read_string buf lexbuf
    }
  | _ { raise (SyntaxError ("Illegal string character: " ^ Lexing.lexeme lexbuf)) }
  | eof { raise (SyntaxError ("String is not terminated")) }
and read_tex_content buf = parse
  | '}'       { TEXCONTENT (Buffer.contents buf) }
  | '\\' '/'  { Buffer.add_char buf '/'; read_tex_content buf lexbuf }
  | '\\' '\\' { Buffer.add_char buf '\\'; read_tex_content buf lexbuf }
  | '\\' 'b'  { Buffer.add_char buf '\b'; read_tex_content buf lexbuf }
  | '\\' 'n'  { Buffer.add_char buf '\n'; read_tex_content buf lexbuf }
  | '\\' 'r'  { Buffer.add_char buf '\r'; read_tex_content buf lexbuf }
  | '\\' 't'  { Buffer.add_char buf '\t'; read_tex_content buf lexbuf }
  | [^ '}' '\\']+
    { Buffer.add_string buf (Lexing.lexeme lexbuf);
      read_tex_content buf lexbuf
    }
  | _ { raise (SyntaxError ("Illegal string character: " ^ Lexing.lexeme lexbuf)) }
  | eof { raise (SyntaxError ("String is not terminated")) }

(*  | ['a'-'z' 'A'-'Z' '>' '-' '<' '='] [ '>' '-' '<' '=' 'a'-'z' 'A'-'Z' '_' '0'-'9']* as lxm {
    	                                                    STRING(lxm)
}*)
