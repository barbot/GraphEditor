type node_type = int

module type GRAPH = sig
  type graph
  type node = private [> `Empty ]
  type arc = private [> `Empty ]
  type attribute_id

  type attribute =
    [ `Check of bool
    | `Choice of string list
    | `Color of string
    | `ControlPoint of DrawingGeom.point
    | `String of string ]

  val iter_node : graph -> (node -> node_type -> unit) -> unit
  val shapes_of_node : graph -> node -> DrawingGeom.shape list

  val get_new_node_choice :
    graph -> ((float * float -> DrawingGeom.shape) * node_type) list

  val new_node : graph -> node_type -> float * float -> node
  val move_node : graph -> float * float -> node -> unit

  val update_node_attribute :
    graph -> node -> attribute_id -> attribute option -> bool

  val get_new_node_attribute :
    graph -> node -> (string * (unit -> attribute_id)) list

  val get_node_attribute :
    graph ->
    node ->
    string * (attribute_id * string * string option * attribute) list

  val remove_node : graph -> node -> unit
  val iter_arc : graph -> (arc -> unit) -> unit

  val shapes_of_arc :
    graph ->
    DrawingGeom.shape ->
    DrawingGeom.shape ->
    arc ->
    DrawingGeom.shape list

  val new_arc : graph -> node -> node -> arc option

  val update_arc_attribute :
    graph -> arc -> attribute_id -> attribute option -> bool

  val get_new_arc_attribute :
    graph -> arc -> (string * (unit -> attribute_id)) list

  val get_arc_attribute :
    graph ->
    arc ->
    string * (attribute_id * string * string option * attribute) list

  val remove_arc : graph -> arc -> unit
  val nodes_of_arc : graph -> arc -> node * node
  val new_graph : unit -> graph
  val read_graph : string -> graph
  val print_graph : (string * (Format.formatter -> graph -> unit) * string) list
end

module Make : functor (G : GRAPH) -> sig
  type selectable_type

  type editor_state = {
    mutable zoom : float;
    mutable origin : float * float;
    mutable selected_obj : selectable_type;
    mutable graph : G.graph;
    mutable new_button_is_set : bool;
    callback : G.graph -> unit;
    canvas : Js_of_ocaml.Dom_html.canvasElement Js_of_ocaml.Js.t;
    attribute_list : Js_of_ocaml.Dom_html.divElement Js_of_ocaml.Js.t;
    ctx : Js_of_ocaml.Dom_html.canvasRenderingContext2D Js_of_ocaml.Js.t;
  }

  val layout_graph : editor_state -> unit
  val draw : editor_state -> float * float -> unit
  val get_exchange_string : G.graph -> string
  val parse_exchange_string : string -> G.graph

  val init :
    ?saveload:#Js_of_ocaml.Dom.node Js_of_ocaml.Js.t ->
    ?slider:Js_of_ocaml.Dom_html.inputElement Js_of_ocaml.Js.t ->
    ?callback:(G.graph -> unit) ->
    ?new_button:Js_of_ocaml.Dom_html.inputElement Js_of_ocaml.Js.t ->
    Js_of_ocaml.Dom_html.canvasElement Js_of_ocaml.Js.t ->
    Js_of_ocaml.Dom_html.divElement Js_of_ocaml.Js.t ->
    editor_state

  val init_client : editor_state -> unit
end
