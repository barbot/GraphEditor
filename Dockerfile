from ocaml/opam:debian-ocaml-4.14

RUN sudo apt-get update -y; sudo apt-get install -y pkg-config libgdbm-dev libgmp-dev libpcre3-dev libssl-dev zlib1g-dev; sudo apt-get clean

RUN eval $(opam env); opam update;
RUN eval $(opam env); opam install js_of_ocaml js_of_ocaml-lwt lwt_ppx js_of_ocaml-ppx js_of_ocaml-ppx_deriving_json xml-light ocamlbuild odoc dune oasis


#ADD ./ graph
#RUN cd graph; eval $(opam env); dune build

#RUN cd editor; eval $(opam env);make; make install

#ADD TikzEditor TikzEditor
#RUN cd TikzEditor; eval $(opam env); make
