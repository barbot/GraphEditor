open Buffer

let b64_of_ui i =
  if i < 26 then char_of_int (i + 65)
  else if i < 52 then char_of_int (i - 26 + 97)
  else if i < 62 then char_of_int (i - 52 + 48)
  else if i = 62 then '+'
  else '/'

let ui_of_b64 c =
  let i = int_of_char c in
  if i >= 65 && i < 91 then i - 65
  else if i >= 97 && i < 123 then i - 97 + 26
  else if i >= 48 && i < 58 then i - 48 + 52
  else if i = 43 then 62
  else 63

let print_buff s =
  String.iter
    (fun c ->
      let i = ui_of_b64 c in
      Printf.printf "[%i]" i)
    s

let buff_int b i =
  if i >= 0 && i < 32 then add_char b (b64_of_ui i)
  else if i >= 0 && i < 1 lsl 10 then (
    let i1 = (i / 32) + 32 and i2 = i mod 32 in
    add_char b (b64_of_ui i1);
    add_char b (b64_of_ui i2))
  else
    let r =
      max 2 ((int_of_float ((log @@ float @@ abs @@ i) /. log 2.0) + 1) / 5)
    in
    (*Printf.printf "r:%i " r;*)
    add_char b
      (b64_of_ui (32 + (if i < 0 then 16 else 0) + (abs i lsr (r * 5))));
    let j = ref (abs i mod (1 lsl (5 * r))) in
    let r2 = ref (r * 5) in
    while !r2 > 0 do
      (* print_string ((string_of_int !r2)^":");
         print_endline (string_of_int !j);*)
      r2 := !r2 - 5;
      add_char b (b64_of_ui ((if !r2 > 0 then 32 else 0) + (!j lsr !r2)));
      j := !j mod (1 lsl !r2)
    done

let int_buff s pos =
  let r = ref 0 in
  while ui_of_b64 s.[pos + !r] >= 32 do
    incr r
  done;
  let i1 = ui_of_b64 s.[pos] in
  if !r = 0 then (pos + 1, i1)
  else if !r = 1 then
    let i2 = ui_of_b64 s.[pos + 1] in
    (pos + 2, (i1 mod (1 lsl 5) * (1 lsl 5)) + i2)
  else
    let j = ref (i1 mod 16) in
    let r2 = ref 1 in
    while !r2 <= !r do
      let i2 = ui_of_b64 s.[pos + !r2] in
      j := (!j lsl 5) + (i2 mod 32);
      incr r2
    done;
    if i1 mod 32 >= 16 then (pos + !r2, - !j) else (pos + !r2, !j)

(*let buff_int b i =
    let ui = (i + (1 lsl 17)) mod (1 lsl 18) in
    let i1 = ui / (1 lsl 12)
    and i2 = (ui / (1 lsl 6)) mod (1 lsl 6)
    and i3 = ui mod (1 lsl 6) in
    add_char b (b64_of_ui i1);
    add_char b (b64_of_ui i2);
    add_char b (b64_of_ui i3)
  let int_buff s pos =
    let i1 = ui_of_b64 s.[pos]
    and i2 = ui_of_b64 s.[pos+1]
    and i3 = ui_of_b64 s.[pos+2] in
    let ui = i1*(1 lsl 12) + i2*(1 lsl 6) +i3 in
    ui - (1 lsl 17)*)

let buff_float b f =
  let i = int_of_float (f *. 100.0) in
  buff_int b i

let float_buff b pos =
  let p, f = int_buff b pos in
  (p, float_of_int f /. 100.)

(*
let check i =
  let t = Buffer.create 10 in
  buff_int t i;
  let str = (Bytes.to_string @@ Buffer.to_bytes t) in
  let p1,i1= int_buff str 0 in
  if i<>i1 then (
    print_buff str;
    print_string  ((string_of_int i)^":");
    print_endline (string_of_int i1);
    assert false
  );;


let _ =
  let i = ref (-1025) in
  while !i < 10000000 do
    check !i;
    incr i;
  done;;

check 1023;;
check 1024;;

check (16383);;
check 16384;;
*)
(*

let _ =
  let t = Buffer.create 100 in
  for i = 16300 to 17000 do
    buff_int t (i);
  done;
  let str = (Bytes.to_string @@ Buffer.to_bytes t) in
  print_buff str;
  print_newline ();
  let pos = ref 0 in
  while !pos < String.length str do
    let p1,i1= int_buff str !pos in
    pos := p1;
    print_string ((string_of_int i1)^"\t");
  done
*)

let buff_string b str =
  let n = String.length str in
  buff_int b ((n + 2) / 3 * 4);
  let pos = ref 0 in
  let pi i = add_char b (b64_of_ui i) in
  while !pos < n do
    let v1 = int_of_char str.[!pos] in
    pi (v1 / 4);
    (if !pos + 1 < n then (
     let v2 = (256 * (v1 mod 4)) + int_of_char str.[!pos + 1] in
     pi (v2 / 16);

     if !pos + 2 < n then (
       let v3 = (256 * (v2 mod 16)) + int_of_char str.[!pos + 2] in
       pi (v3 / 64);
       pi (v3 mod 64))
     else
       let vp3 = (256 * (v2 mod 16)) + 0 in
       pi (vp3 / 64);
       add_char b '=')
    else
      let vp2 = (256 * (v1 mod 4)) + 0 in
      pi (vp2 / 16);
      add_char b '=';
      add_char b '=');
    pos := !pos + 3
  done

let string_buff b pin =
  let p, n = int_buff b pin in
  let pos = ref p in
  let pos2 = ref 0 in
  let str = Bytes.create (n / 4 * 3) in
  while !pos < n + p do
    let v1 = ui_of_b64 b.[!pos] in
    let v2 = ui_of_b64 b.[!pos + 1] in
    Bytes.set str !pos2 @@ char_of_int ((v1 * 4) + (v2 / 16));
    if b.[!pos + 2] <> '=' then
      let v3 = ui_of_b64 b.[!pos + 2] in
      if b.[!pos + 3] <> '=' then (
        let v4 = ui_of_b64 b.[!pos + 3] in
        Bytes.set str (!pos2 + 1) @@ char_of_int ((v2 mod 16 * 16) + (v3 / 4));
        Bytes.set str (!pos2 + 2) @@ char_of_int ((v3 mod 4 * 64) + v4);
        pos2 := !pos2 + 3)
      else (
        Bytes.set str (!pos2 + 1) @@ char_of_int ((v2 mod 16 * 16) + (v3 / 4));
        pos2 := !pos2 + 2)
    else pos2 := !pos2 + 1;
    pos := !pos + 4
  done;
  (!pos, Bytes.sub_string str 0 !pos2)

(*let t = Buffer.create 1000 in
    buff_int t (-5024);
    buff_string t "Mandsflsdh sdlhkfsd sd h % ^& # ";
    let str = Bytes.to_string @@ Buffer.to_bytes t in
    print_endline str;
    let p,i = int_buff str 0 in
    print_endline (string_of_int i);
    print_endline (string_of_int p);
    string_buff str p*)

let buff_list b f l =
  List.iter
    (fun x ->
      Buffer.add_char b 'l';
      f b x)
    l

let rec list_buff b f pos =
  if pos < String.length b && b.[pos] = 'l' then
    let p, le = f b (pos + 1) in
    let p2, lq = list_buff b f p in
    (p2, le :: lq)
  else (pos, [])

let id l =
  let t = Buffer.create 10 in
  buff_list t buff_int l;
  let s = Bytes.to_string @@ Buffer.to_bytes t in
  let l2 = snd @@ list_buff s int_buff 0 in
  l = l2

let buff_attribute b = function
  | `Color c ->
      Buffer.add_char b 'c';
      buff_string b c
  | `ControlPoint (f1, f2) ->
      Buffer.add_char b 'P';
      buff_float b f1;
      buff_float b f2
  | `String s ->
      Buffer.add_char b 'S';
      buff_string b s
  | `Choice sl ->
      Buffer.add_char b 'd';
      buff_list b buff_string sl;
      Buffer.add_char b 'd'
  | `Check true ->
      Buffer.add_char b 'B';
      Buffer.add_char b 't'
  | `Check false ->
      Buffer.add_char b 'B';
      Buffer.add_char b 'f'

let attribute_buff b pos =
  match b.[pos] with
  | 'c' ->
      let p, s = string_buff b (pos + 1) in
      (p, `Color s)
  | 'P' ->
      let p1, f1 = float_buff b (pos + 1) in
      let p2, f2 = float_buff b p1 in
      (p2, `ControlPoint (f1, f2))
  | 'S' ->
      let p, s = string_buff b (pos + 1) in
      (p, `String s)
  | 'd' ->
      let p, s = list_buff b string_buff (pos + 1) in
      assert (b.[p] = 'd');
      (p + 1, `Choice s)
  | 'B' when b.[pos + 1] = 't' -> (pos + 2, `Check true)
  | 'B' when b.[pos + 1] = 'f' -> (pos + 2, `Check false)
  | _ -> failwith "bad serialization"

let write_attribute b (_, s, a) =
  add_char b 'T';
  buff_string b s;
  buff_attribute b a

let read_attribute b pos =
  assert (b.[pos] = 'T');
  let pos2, s = string_buff b (pos + 1) in
  let pos3, at = attribute_buff b pos2 in
  (pos3, (s, at))
