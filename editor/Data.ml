exception Empty
exception Destroyed

type ('a, 'b, 'k) t = {
  mutable table : ('a * 'b) option array;
  mutable hash : ('a, int) Hashtbl.t;
  mutable size : int;
}

type 'k key = int

let fod = function Some a -> a | None -> raise Destroyed
let create () = { table = [||]; hash = Hashtbl.create 10; size = 0 }
let acca t i = if Array.length t.table = 0 then raise Empty else fod t.table.(i)
let index t s = Hashtbl.find t.hash s
let acc t s = index t s |> acca t |> snd

let updatea i v t =
  if Array.length t.table = 0 then raise Empty
  else
    let a, _ = fod t.table.(i) in
    t.table.(i) <- Some (a, v)

let remove t i =
  if Array.length t.table = 0 then raise Empty
  else
    match t.table.(i) with
    | Some (a, _) ->
        t.table.(i) <- None;
        Hashtbl.remove t.hash a
    | None -> ()

let rec addk (a, b) t =
  match Array.length t.table with
  | 0 ->
      t.table <- Array.make 10 (Some (a, b));
      t.size <- 1;
      Hashtbl.add t.hash a 0;
      0
  | s when s > t.size ->
      t.table.(t.size) <- Some (a, b);
      Hashtbl.add t.hash a t.size;
      t.size <- t.size + 1;
      t.size - 1
  | _ ->
      let t2 = Array.make (2 * t.size) t.table.(0) in
      Array.blit t.table 0 t2 0 t.size;
      t.table <- t2;
      addk (a, b) t

let add a t = ignore (addk a t)

let foldi f e t =
  match Array.length t.table with
  | 0 -> e
  | _ ->
      let buff = ref e in
      for i = 0 to t.size - 1 do
        match t.table.(i) with Some v -> buff := f !buff i v | None -> ()
      done;
      !buff

let reduce f t =
  match Array.length t.table with
  | 0 -> None
  | _ ->
      if t.size = 0 then None
      else
        let j = ref 0 in
        while !j < t.size && t.table.(!j) = None do
          incr j
        done;
        let buff = ref (fod t.table.(!j)) in
        for i = !j + 1 to t.size - 1 do
          (* Here we cannot used t.(i) in case f reschedules the table*)
          buff := f !buff (acca t i)
        done;
        Some !buff

let fold f e t = foldi (fun b _ a -> f b a) e t
let iteri f t = foldi (fun _ i a -> f i a) () t
let iter f t = foldi (fun _ _ a -> f a) () t
let adds t1 t2 = iter (fun x -> add x t1) t2

let map f t1 =
  let t2 = create () in
  iter (fun (x, y) -> add (f x y) t2) t1;
  t2

let filter p t1 =
  let t2 = create () in
  iter (fun (x, y) -> if p x y then add (x, y) t2) t1;
  t2

let copy t1 = map (fun x y -> (x, y)) t1
let size t = t.size
let sample d = if Array.length d.table > 0 then fod d.table.(0) else raise Empty

let unsafe x =
  assert (x >= 0);
  x

let unsafe_rev x = x
