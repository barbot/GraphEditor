open Js_of_ocaml
open GraphEditor
module MCGraphEd = GraphDrawing.Make (SimpleGraph.S (MarkovChain))

let _ =
  UtilsWeb.run' @@ fun () ->
  match Dom_html.(getElementById_coerce "mc_drawing" CoerceTo.canvas) with
  | Some canvas ->
      let ed =
        MCGraphEd.init
          ?slider:Dom_html.(getElementById_coerce "zoomslider" CoerceTo.input)
          ~saveload:Dom_html.(getElementById "saveload")
          canvas
          Dom_html.(getElementById "attr_list")
      in
      (*ed.graph <- PTGraphEd.parse_exchange_string "";*)
      ed.callback ed.graph;

      MCGraphEd.init_client ed
  | None -> failwith "no graph"
