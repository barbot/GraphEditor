open Js_of_ocaml
open DrawingGeom.Point

type node_type = int

module type GRAPH = sig
  type graph
  type node = private [> `Empty ]
  type arc = private [> `Empty ]
  type attribute_id

  type attribute =
    [ `String of string
    | `Choice of string list
    | `Check of bool
    | `ControlPoint of DrawingGeom.point
    | `Color of string ]

  val iter_node : graph -> (node -> node_type -> unit) -> unit
  val shapes_of_node : graph -> node -> DrawingGeom.shape list

  val get_new_node_choice :
    graph -> ((DrawingGeom.point -> DrawingGeom.shape) * node_type) list

  val new_node : graph -> node_type -> DrawingGeom.point -> node
  val move_node : graph -> float * float -> node -> unit

  val update_node_attribute :
    graph -> node -> attribute_id -> attribute option -> bool

  val get_new_node_attribute :
    graph -> node -> (string * (unit -> attribute_id)) list

  val get_node_attribute :
    graph ->
    node ->
    string * (attribute_id * string * string option * attribute) list

  val remove_node : graph -> node -> unit
  val iter_arc : graph -> (arc -> unit) -> unit

  val shapes_of_arc :
    graph ->
    DrawingGeom.shape ->
    DrawingGeom.shape ->
    arc ->
    DrawingGeom.shape list

  val new_arc : graph -> node -> node -> arc option

  val update_arc_attribute :
    graph -> arc -> attribute_id -> attribute option -> bool

  val get_new_arc_attribute :
    graph -> arc -> (string * (unit -> attribute_id)) list

  val get_arc_attribute :
    graph ->
    arc ->
    string * (attribute_id * string * string option * attribute) list

  val remove_arc : graph -> arc -> unit
  val nodes_of_arc : graph -> arc -> node * node
  val new_graph : unit -> graph
  val read_graph : string -> graph
  val print_graph : (string * (Format.formatter -> graph -> unit) * string) list
end

module Make (G : GRAPH) = struct
  open DrawingGeom
  open UtilsWeb

  let width = 4096
  let height = 2048
  let increment = 5

  type selectable_type =
    | Empty
    | Node of G.node
    | Arc of G.arc
    | Area of DrawingGeom.Rectangle.t

  type editor_state = {
    mutable zoom : float;
    mutable origin : DrawingGeom.point;
    mutable selected_obj : selectable_type;
    mutable graph : G.graph;
    mutable new_button_is_set : bool;
    callback : G.graph -> unit;
    canvas : Js_of_ocaml.Dom_html.canvasElement Js.t;
    attribute_list : Js_of_ocaml.Dom_html.divElement Js.t;
    ctx : Dom_html.canvasRenderingContext2D Js.t;
  }

  let center_of_node s node =
    DrawingGeom.center_shapes @@ G.shapes_of_node s.graph node

  let node_in_rect s rect =
    let nlist = ref [] in
    G.iter_node s.graph (fun node _ -> nlist := node :: !nlist);
    !nlist
    |> List.map (fun node -> (center_of_node s node, node))
    |> Rectangle.contains rect

  let download content name mimetype =
    let c = Js.string content
    and n = Js.string name
    and m = Js.string mimetype in
    Js.Unsafe.fun_call
      (Js.Unsafe.js_expr "download")
      [| Js.Unsafe.inject c; Js.Unsafe.inject n; Js.Unsafe.inject m |]

  let to_screen s (x, y) =
    let xo, yo = s.origin in
    let x2 = (x -. xo) *. s.zoom and y2 = (y -. yo) *. s.zoom in
    (x2, y2)

  let from_screen s (x, y) =
    let xo, yo = s.origin in
    ((x /. s.zoom) +. xo, (y /. s.zoom) +. yo)

  let stick_coord s ~exact pos =
    let x, y = from_screen s pos in
    let i = if exact then 1.0 else float increment in
    let f z = i *. floor ((z /. i) +. 0.5) in
    to_screen s (f x, f y)

  let clean s =
    let ctx = s.ctx in
    (*let color = CSS.Color.string_of_t (CSS.Color.rgb 220 220 220) in
      ctx##.fillStyle := (Js.string  color);
      ctx##(fillRect 0.0 0.0 (float width) (float height));*)
    ctx
    ## (clearRect (Js.float 0.0) (Js.float 0.0)
          (Js.float @@ float width)
          (Js.float @@ float height));
    let colors = CSS.Color.string_of_t (CSS.Color.rgb 0 0 0) in
    ctx##.strokeStyle := Js.string colors

  let set_style ctx is_selected is_over =
    let open DrawingGeom.Canvas in
    let color =
      if is_selected then (250, 10, 10)
      else if is_over then (10, 10, 250)
      else (10, 10, 10)
    in
    setFillColor ctx (255, 255, 255);
    setStrokeColor ctx (0, 0, 0);
    setAmbiant ctx color

  let shapes_of_arc s obj =
    let source, target = G.nodes_of_arc s.graph obj in
    let source_sh = G.shapes_of_node s.graph source |> DrawingGeom.tangible
    and target_sh = G.shapes_of_node s.graph target |> DrawingGeom.tangible in
    let shapes = G.shapes_of_arc s.graph source_sh target_sh obj in
    shapes

  let shape_of_control_point p = `Rectangle (p, 1.0, 1.5, 0.78540)

  (*`Circle (p,1.7)*)

  let layout_graph s =
    let nodes = Hashtbl.create 10 in
    let cmp = ref 0 in
    G.iter_node s.graph (fun node _ ->
        let center =
          DrawingGeom.center_shapes @@ G.shapes_of_node s.graph node
        in
        Hashtbl.add nodes node (!cmp, center);
        incr cmp);
    let arcs = ref [] in
    G.iter_arc s.graph (fun arc ->
        let source, target = G.nodes_of_arc s.graph arc in
        arcs :=
          ( arc,
            fst @@ Hashtbl.find nodes source,
            fst @@ Hashtbl.find nodes target )
          :: !arcs);
    let nodearray = Array.make !cmp (`Empty, (0.0, 0.0)) in
    Hashtbl.iter (fun node (i, pos) -> nodearray.(i) <- (node, pos)) nodes;
    let new_layout = Layout.layout_graph nodearray !arcs in
    Array.iter (fun (node, pos) -> G.move_node s.graph pos node) new_layout

  let draw s mouse_pos =
    clean s;
    let found_node = ref false in
    G.iter_node s.graph (fun obj _ ->
        let shapes = G.shapes_of_node s.graph obj in
        let over_node = is_over_shapes (to_screen s) mouse_pos shapes in
        found_node := !found_node || over_node;
        let node_selected =
          match s.selected_obj with
          | Node obj2 when obj = obj2 -> true
          | Area r ->
              []
              <> DrawingGeom.Rectangle.contains r [ (center_of_node s obj, ()) ]
          | _ -> false
        in
        set_style s.ctx node_selected over_node;
        List.iter (fun sh -> draw_shape (to_screen s) s.ctx sh) shapes);
    G.iter_arc s.graph (fun obj ->
        let shapes = shapes_of_arc s obj in
        let over_arc = is_over_shapes (to_screen s) mouse_pos shapes in
        set_style s.ctx (s.selected_obj = Arc obj) (over_arc && not !found_node);
        draw_shapes (to_screen s) s.ctx ~thick:2.0 shapes;
        if s.selected_obj = Arc obj then
          G.get_arc_attribute s.graph obj
          |> snd
          |> List.iter (function
               | _, _, _, `ControlPoint p ->
                   let sh = shape_of_control_point p in
                   set_style s.ctx false
                     (is_over_shape (to_screen s) mouse_pos sh);
                   draw_shapes (to_screen s) s.ctx [ sh ]
               | _ -> ()))

  let filter_attr a = List.map (fun (a, b, _, d) -> (a, b, d)) @@ snd a

  let get_exchange_string graph =
    let open GenericSerializer in
    let buff = Buffer.create 20 in
    Buffer.add_string buff "GEX";
    let map = Hashtbl.create 10 in
    let i = ref 0 in
    G.iter_node graph (fun node node_type ->
        Hashtbl.add map node !i;
        incr i;
        Buffer.add_char buff 'N';
        Buffer.add_char buff (b64_of_ui node_type);
        let f1, f2 = DrawingGeom.center_shapes (G.shapes_of_node graph node) in
        buff_float buff f1;
        buff_float buff f2;
        let attr = filter_attr @@ G.get_node_attribute graph node in
        buff_list buff write_attribute attr);
    G.iter_arc graph (fun arc ->
        print_endline "new arc";
        let n1, n2 = G.nodes_of_arc graph arc in
        Buffer.add_char buff 'A';
        buff_int buff (Hashtbl.find map n1);
        buff_int buff (Hashtbl.find map n2);
        let attr = filter_attr @@ G.get_arc_attribute graph arc in
        buff_list buff write_attribute attr);
    let def_attr = filter_attr @@ G.get_arc_attribute graph `Empty in
    Buffer.add_char buff 'D';
    buff_list buff write_attribute def_attr;

    Bytes.to_string @@ Buffer.to_bytes buff

  let rec find_and_remove x = function
    | [] -> (None, [])
    | (tx, ty) :: q when tx = x -> (Some ty, q)
    | t :: q ->
        let v, q2 = find_and_remove x q in
        (v, t :: q2)

  let parse_exchange_node graph str pos node =
    let open GenericSerializer in
    let npos, attrl = list_buff str read_attribute pos in
    let cattr = filter_attr @@ G.get_node_attribute graph node in
    let attrl2 =
      List.fold_left
        (fun _ (id, name, _) ->
          let found, nl = find_and_remove name attrl in
          (match found with
          | Some v ->
              if not (G.update_node_attribute graph node id (Some v)) then
                print_endline ("Fail to update default attribute:" ^ str)
          | None -> ());
          nl)
        attrl cattr
    in

    List.iter
      (fun (str, attr) ->
        match List.assoc_opt str (G.get_new_node_attribute graph node) with
        | None -> print_endline ("Fail to create attribute:" ^ str)
        | Some f ->
            let nat = f () in
            if not (G.update_node_attribute graph node nat (Some attr)) then
              print_endline ("Fail to update attribute:" ^ str))
      attrl2;
    npos

  let parse_exchange_arc graph str pos n1 n2 =
    let open GenericSerializer in
    let npos, attrl = list_buff str read_attribute pos in
    (match G.new_arc graph n1 n2 with
    | None -> print_endline "Fail to add arc"
    | Some arc ->
        let cattr = filter_attr @@ G.get_arc_attribute graph arc in
        let attrl2 =
          List.fold_left
            (fun _ (id, name, _) ->
              let found, nl = find_and_remove name attrl in
              (match found with
              | Some v ->
                  if not (G.update_arc_attribute graph arc id (Some v)) then
                    print_endline ("Fail to update default attribute:" ^ name)
              | None -> ());
              nl)
            attrl cattr
        in

        List.iter
          (fun (str, attr) ->
            match List.assoc_opt str (G.get_new_arc_attribute graph arc) with
            | None -> print_endline ("Fail to create attribute:" ^ str)
            | Some f ->
                let nat = f () in
                if not (G.update_arc_attribute graph arc nat (Some attr)) then
                  print_endline ("Fail to update attribute:" ^ str))
          attrl2);
    npos

  let parse_exchange_string str =
    let open GenericSerializer in
    let graph = G.new_graph () in
    let length = String.length str in
    assert (String.sub str 0 3 = "GEX");
    let pos = ref 3 in
    let map = Hashtbl.create 10 in
    let i = ref 0 in
    while !pos + 4 <= length && str.[!pos] = 'N' do
      let node_type = ui_of_b64 str.[!pos + 1] in
      let p1, f1 = float_buff str (!pos + 2) in
      let p2, f2 = float_buff str p1 in
      let node = G.new_node graph node_type (f1, f2) in
      Hashtbl.add map !i node;
      incr i;
      pos := parse_exchange_node graph str p2 node
    done;
    (*print_endline (String.sub str !pos (String.length str - !pos));*)
    while !pos + 7 <= length && str.[!pos] = 'A' do
      let p1, nid1 = int_buff str (!pos + 1) in
      let p2, nid2 = int_buff str p1 in
      let n1 = Hashtbl.find map nid1 in
      let n2 = Hashtbl.find map nid2 in
      pos := parse_exchange_arc graph str p2 n1 n2
      (*print_endline (String.sub str !pos (String.length str - !pos))*)
    done;
    pos := parse_exchange_node graph str (!pos + 1) `Empty;
    graph

  let save_load_html s =
    let loadfile st =
      s.graph <-
        (if String.sub st 0 3 = "GEX" then parse_exchange_string st
         else G.read_graph st);
      draw s (0.0, 0.0)
    in

    let dl =
      ( "GEX",
        fun () ->
          let string_value = get_exchange_string s.graph in
          download string_value "graph.gex" "data:application/xml" )
      :: List.map
           (fun (x, df, fn) ->
             ( x,
               fun () ->
                 let string_value = Format.asprintf "%a" df s.graph in
                 download string_value fn "data:application/xml" ))
           G.print_graph
    in
    div
      [
        p
          [
            txt "Load file: ";
            (*input ~a:[a_id "filein"; a_input_type `File; a_onchange (change_file loadfile)] ();*)
            text_input ~_type:"file" ~id:"filein"
              ~on_change:(change_file loadfile) "";
            choice_input ~init_value:(Some "Download")
              ~on_change:(fun x -> (List.assoc x dl) ())
              (List.map fst dl);
            (*button ~a:[a_onclick (fun _ ->
                           let string_value = Format.asprintf "%a" G.print_graph s.graph in                            download string_value (G.download_file_name s.graph) "data:application/xml") ]
              [pcdata "Download"];*)
            (*button ~a:[a_onclick (fun _ ->
                           let string_value = get_exchange_string s.graph in
                           download string_value "graph.gex" "data:application/xml") ]
              [pcdata "Get GEX"];*)
            button
              ~on_click:(fun _ ->
                layout_graph s;
                draw s (0.0, 0.0))
              [ txt "Layout Graph" ];
            update_link "Direct Link" (fun n ->
                n ^ "?q=" ^ get_exchange_string s.graph);
          ];
      ]

  let init ?saveload ?slider ?callback ?new_button canvas attribute_list =
    (*let init canvas_elt attr_list_div saveload_elt slider_elt =
      let canvas = Eliom_content.Html.To_dom.of_canvas canvas_elt in
      let attribute_list = Eliom_content.Html.To_dom.of_div attr_list_div in
      let saveload = Eliom_content.Html.To_dom.of_div saveload_elt in
      let slider = Eliom_content.Html.To_dom.of_input slider_elt in*)
    let ctx = canvas ## (getContext Dom_html._2d_) in

    ctx##.lineCap := Js.string "round";
    ctx##.font := Js.string "15px Arial";
    let s =
      {
        zoom = 1.5;
        new_button_is_set = false;
        origin = (-50.0, -50.0);
        selected_obj = Empty;
        graph = G.new_graph ();
        callback = (match callback with None -> ignore | Some s -> s);
        canvas;
        ctx;
        attribute_list;
      }
    in
    (try
       match getURL "q" with
       | Some str ->
           if String.sub str 0 3 = "GEX" then
             s.graph <- parse_exchange_string str
       | None -> ()
     with _ -> ());
    (match slider with
    | Some sl ->
        sl##.value := Js.string @@ string_of_float @@ ((s.zoom -. 0.2) *. 50.);
        sl##.oninput :=
          Dom.handler (fun _ ->
              s.zoom <-
                0.2 +. ((float_of_string @@ Js.to_string sl##.value) /. 50.0);
              draw s (0.0, 0.0);
              Js._true)
    | None -> ());
    (match new_button with
    | Some nb ->
        nb##.oninput :=
          Dom.handler (fun _ ->
              s.new_button_is_set <- Js.to_bool nb##.checked;
              Js._true)
    | _ -> ());
    (match saveload with
    | Some div ->
        Dom.appendChild div
          ((*Eliom_content.Html.To_dom.of_div*) save_load_html s)
    | None -> ());
    s

  let draw_choice s (x, y) =
    let color = CSS.Color.string_of_t (CSS.Color.rgb 50 255 50) in
    let l = G.get_new_node_choice s.graph in
    let n = List.length l in
    let rad = match n with 1 -> 0.0 | 2 -> 20.0 | 3 -> 26.0 | _ -> 30.0 in
    List.iteri
      (fun i (fd, _) ->
        let x2, y2 =
          (x, y) +.. rot (float i /. float n *. 2.0 *. pi) (rad, 0.0)
        in
        let pos3 =
          (x, y) +.. rot ((0.5 +. float i) /. float n *. 2.0 *. pi) (rad, 0.0)
        in
        s.ctx##.strokeStyle := Js.string color;
        s.ctx##.lineWidth := Js.float 2.0;
        s.ctx##beginPath;
        s.ctx ## (moveTo (Js.float x) (Js.float y));
        s.ctx ## (lineTo (Js.float x2) (Js.float y2));
        s.ctx##stroke;
        let shape = fd (from_screen s pos3) in
        draw_shape (to_screen s) s.ctx shape)
      l;
    List.map (fun (_, id) -> id) l

  let which_choice clist pos =
    let n = List.length clist in
    let rho = mod_float ((1.0 *. pi) +. angle pos) (2.0 *. pi) in
    let base = 2.0 *. pi /. float n in
    List.nth clist (int_of_float (rho /. base))

  let get_coord ?(exact = false) s ev =
    let x0, y0 = Dom_html.elementClientPosition s.canvas in
    let clientX = Js.float_of_number ev##.clientX in
    let clientY = Js.float_of_number ev##.clientY in
    stick_coord s ~exact (clientX -. float x0, clientY -. float y0)

  let is_over_object s mouse_pos =
    let obj = ref Empty in

    G.iter_node s.graph (fun o _ ->
        let shapes = G.shapes_of_node s.graph o in
        if is_over_shapes (to_screen s) mouse_pos shapes then obj := Node o);
    if !obj = Empty then
      G.iter_arc s.graph (fun o ->
          let shapes = shapes_of_arc s o in
          if is_over_shapes (to_screen s) mouse_pos shapes then obj := Arc o);
    !obj

  let rec html_of_attr s attr nattr cb =
    let callback id oldv newv =
      let nva =
        match oldv with
        | `ControlPoint pt -> `ControlPoint pt
        | `Choice _ -> `Choice [ newv ]
        | `Color _ -> `Color newv
        | `String _ -> `String newv
        | `Check _ -> `Check (bool_of_string newv)
      in
      let v =
        match s.selected_obj with
        | Node node ->
            let v2 = G.update_node_attribute s.graph node id (Some nva) in
            if v2 then (
              try ignore @@ G.get_node_attribute s.graph node
              with _ ->
                s.selected_obj <- Empty;
                update_attr s);
            v2
        | Arc arc ->
            let v2 = G.update_arc_attribute s.graph arc id (Some nva) in
            if v2 then (
              try ignore @@ G.get_arc_attribute s.graph arc
              with _ ->
                s.selected_obj <- Empty;
                update_attr s);
            v2
        | Empty -> G.update_arc_attribute s.graph `Empty id (Some nva)
        | Area _ -> false
      in
      s.callback s.graph;
      draw s (0.0, 0.0);
      v
    in
    let callback_supr id _ =
      let _ =
        match s.selected_obj with
        | Node node -> G.update_node_attribute s.graph node id None
        | Arc arc -> G.update_arc_attribute s.graph arc id None
        | Empty -> G.update_arc_attribute s.graph `Empty id None
        | Area _ -> false
      in
      draw s (0.0, 0.0);
      update_attr s
    in
    let attr_tr =
      List.map
        (function
          | id, n, title, v -> (
              let delbutton =
                button ~on_click:(callback_supr id)
                  [
                    (*span ~a:[a_class ["glyphicon";"glyphicon-remove"]] [];*)
                    txt "X";
                  ]
              in
              match v with
              | `ControlPoint (x, y) ->
                  tr
                    [
                      td ?title [ txt n; txt (Printf.sprintf ": (%g,%g)" x y) ];
                      td [];
                      td [ delbutton ];
                    ]
              | `Check b ->
                  tr
                    [
                      td ?title [ txt n; txt ": " ];
                      td [ check_input ~on_change:(callback id v) b ];
                      td [ delbutton ];
                    ]
              | `Choice (_ :: _ as l) ->
                  tr
                    [
                      td ?title [ txt n; txt ": " ];
                      td
                        [
                          choice_input
                            ~on_change:(fun nv -> ignore @@ callback id v nv)
                            l;
                        ];
                      td [ delbutton ];
                    ]
              | `Choice [] -> tr []
              | `String str ->
                  tr
                    [
                      td ?title [ txt n; txt ": " ];
                      td [ text_input ~on_change:(callback id v) str ];
                      td [ delbutton ];
                    ]
              | `Color str ->
                  tr
                    [
                      td ?title [ txt n; txt ": " ];
                      td [ color_input ~on_change:(callback id v) str ];
                      td [ delbutton ];
                    ]))
        attr
    in
    let new_attr =
      tr
        [
          td
            [
              choice_input ~init_value:(Some "New Attribute")
                ~on_change:(fun x -> cb x)
                nattr;
            ];
          td [];
        ]
    in
    attr_tr @ if nattr <> [] then [ new_attr ] else []

  and set_selected s sobj mouse_pos =
    s.selected_obj <- sobj;
    draw s mouse_pos;
    update_attr s

  and update_attr s =
    let name, attr =
      match s.selected_obj with
      | Node node -> G.get_node_attribute s.graph node
      | Arc arc -> G.get_arc_attribute s.graph arc
      | Empty -> G.get_arc_attribute s.graph `Empty
      | Area _ -> ("Selection", [])
    in

    let a = Dom.list_of_nodeList s.attribute_list##.childNodes in
    List.iter (fun x -> Dom.removeChild s.attribute_list x) a;

    let divat = div [ txt name ] in
    Dom.appendChild s.attribute_list divat;

    let nattr =
      match s.selected_obj with
      | Node node -> G.get_new_node_attribute s.graph node
      | Arc arc -> G.get_new_arc_attribute s.graph arc
      | Empty -> G.get_new_arc_attribute s.graph `Empty
      | Area _ -> []
    in
    let nsattr, _ = List.split nattr in

    let new_attr_callback so =
      let v = List.assoc so nattr in
      ignore @@ v ();
      draw s (0.0, 0.0);
      update_attr s
    in

    let slHTML = table (html_of_attr s attr nsattr new_attr_callback) in

    Dom.appendChild s.attribute_list slHTML;
    Dom.appendChild s.attribute_list (br ());

    if match s.selected_obj with Empty -> false | _ -> true then
      let suprcb _ =
        (match s.selected_obj with
        | Empty -> ()
        | Node node -> G.remove_node s.graph node
        | Arc arc -> G.remove_arc s.graph arc
        | Area r ->
            let l = node_in_rect s r in
            List.iter (fun node -> G.remove_node s.graph node) l);
        s.callback s.graph;
        set_selected s Empty (0.0, 0.0)
      in
      Dom.appendChild s.attribute_list
        (button ~on_click:suprcb [ txt "Delete" ])

  let init_client s =
    s.ctx##.lineCap := Js.string "round";
    draw s (0.0, 0.0);

    let open Lwt in
    let open Js_of_ocaml_lwt.Lwt_js_events in
    let osobj = ref Empty in
    Lwt.async (fun () ->
        Lwt.pick
          [
            (* highlight element *)
            mousemoves s.canvas (fun ev _ ->
                let mouse_pos = get_coord ~exact:true s ev in
                let sobj = is_over_object s mouse_pos in
                if sobj <> !osobj then (
                  draw s mouse_pos;
                  osobj := sobj);
                Lwt.return ());
            (* mouse down *)
            mousedowns s.canvas (fun ev _ ->
                let mouse_pos = get_coord ~exact:true s ev in
                let sobj = is_over_object s mouse_pos in

                let cp =
                  match s.selected_obj with
                  | Arc arc ->
                      G.get_arc_attribute s.graph arc
                      |> snd
                      |> List.filter (function
                           | _, _, _, `ControlPoint _ -> true
                           | _ -> false)
                      |> List.map (function
                           | id, n, _, `ControlPoint p ->
                               (id, n, shape_of_control_point p)
                           | _ -> failwith "notpossible")
                      |> List.filter (function _, _, sh ->
                             is_over_shape (to_screen s) mouse_pos sh)
                  | _ -> []
                in

                let create_new =
                  ev##.shiftKey = Js._true
                  || ev##.button = 2
                  (*Right click*) || s.new_button_is_set
                in

                (match (s.selected_obj, sobj) with
                | Area r, Node n
                  when List.mem n (node_in_rect s r) && not create_new ->
                    ()
                | Arc _, _ when cp <> [] -> ()
                | _ -> set_selected s sobj mouse_pos);
                match s.selected_obj with
                (* New node *)
                | Empty when create_new ->
                    let nchoice = draw_choice s mouse_pos in
                    mouseup s.canvas >>= fun ev2 ->
                    let vect = mouse_pos -.. get_coord ~exact:true s ev2 in
                    let nn =
                      G.new_node s.graph
                        (which_choice nchoice vect)
                        (from_screen s (get_coord s ev))
                    in
                    s.callback s.graph;
                    set_selected s (Node nn) (get_coord s ev);
                    Lwt.return ()
                (* Select area *)
                | Empty ->
                    Lwt.pick
                      [
                        ( mouseup s.canvas >>= fun ev ->
                          let mouse_pos = get_coord ~exact:true s ev in
                          draw s mouse_pos;
                          Lwt.return () );
                        mousemoves s.canvas (fun ev _ ->
                            let mouse_pos2 = get_coord ~exact:true s ev in
                            let pos1 = from_screen s mouse_pos in
                            let pos2 = from_screen s mouse_pos2 in
                            let rect = Rectangle.of_border pos1 pos2 in
                            s.selected_obj <- Area rect;
                            draw s mouse_pos2;
                            set_style s.ctx false true;
                            s.ctx##.fillStyle :=
                              Js.string "rgba(0, 0, 255, 0.3)";
                            draw_shapes (to_screen s) s.ctx [ `Rectangle rect ];
                            Lwt.return ());
                      ]
                (* new Arc *)
                | Node node1 when create_new ->
                    Lwt.pick
                      [
                        ( mouseup s.canvas >>= fun ev ->
                          let mouse_pos = get_coord ~exact:true s ev in
                          let nsobj = is_over_object s mouse_pos in
                          (match nsobj with
                          | Node node2 ->
                              ignore @@ G.new_arc s.graph node1 node2
                          | _ -> ());
                          s.callback s.graph;
                          draw s mouse_pos;
                          Lwt.return () );
                        mousemoves s.canvas (fun ev _ ->
                            let mouse_pos2 = get_coord ~exact:true s ev in
                            draw s mouse_pos2;
                            set_style s.ctx true false;
                            let pos1 = from_screen s mouse_pos2 in
                            let shape =
                              G.shapes_of_node s.graph node1
                              |> DrawingGeom.tangible
                            in
                            let pos2 = projection_shape pos1 shape in
                            draw_shapes (to_screen s) s.ctx ~thick:2.0
                              [ `Line (pos2, pos1); `Arrow (pos1, pos2) ];
                            Lwt.return ());
                      ]
                (* Move on node *)
                | Node node ->
                    Lwt.pick
                      [
                        (mouseup s.canvas >>= fun _ -> Lwt.return ());
                        mousemoves s.canvas (fun ev _ ->
                            let mouse_pos = get_coord s ev in
                            G.move_node s.graph (from_screen s mouse_pos) node;
                            draw s mouse_pos;
                            Lwt.return ());
                      ]
                (* Move control point *)
                | Arc arc -> (
                    match cp with
                    | [] -> Lwt.return ()
                    | (id, _, _) :: _ ->
                        Lwt.pick
                          [
                            mousemoves s.canvas (fun ev _ ->
                                let nmouse_pos = get_coord s ev in
                                let pt = from_screen s nmouse_pos in
                                ignore
                                @@ G.update_arc_attribute s.graph arc id
                                     (Some (`ControlPoint pt));
                                draw s nmouse_pos;
                                update_attr s;
                                Lwt.return ());
                            (mouseup s.canvas >>= fun _ -> Lwt.return ());
                          ])
                (* Move several node*)
                | Area r ->
                    let ls = node_in_rect s r in
                    let base_node =
                      match sobj with Node n -> n | _ -> List.hd ls
                    in
                    let cpointlistref = ref [] in
                    let init_mouse = from_screen s mouse_pos in
                    G.iter_arc s.graph (fun arc ->
                        G.get_arc_attribute s.graph arc
                        |> snd
                        |> List.iter (function
                             | id, _, _, `ControlPoint p
                               when DrawingGeom.Rectangle.contain r p ->
                                 cpointlistref :=
                                   (id, p -.. init_mouse, arc) :: !cpointlistref
                             | _ -> ()));

                    Lwt.pick
                      [
                        (mouseup s.canvas >>= fun _ -> Lwt.return ());
                        mousemoves s.canvas (fun ev _ ->
                            let mouse_pos = get_coord s ev in
                            let pos1 = from_screen s mouse_pos in
                            let vect = pos1 -.. center_of_node s base_node in
                            List.iter
                              (fun node ->
                                let pos2 = center_of_node s node in
                                G.move_node s.graph (pos2 +.. vect) node)
                              ls;
                            List.iter
                              (fun (id, p, arc) ->
                                ignore
                                @@ G.update_arc_attribute s.graph arc id
                                     (Some (`ControlPoint (p +.. pos1))))
                              !cpointlistref;
                            draw s mouse_pos;
                            Lwt.return ());
                      ]);
          ])
end
